<?php

namespace glowbug\mailer;

use Exception;

class MailPart extends MailBase {
    /// encoding types
    const ENC_7BIT = '7bit';
    const ENC_8BIT = '8bit';
    const ENC_B64  = 'base64';
    const ENC_BIN  = 'binary';
    const ENC_QP   = 'quoted-printable';

    /**
     * @var string content of the mail part.
     */
    public string $content = '';

    /**
     * @var string filename of an (inline) attachment.
     */
    public string $filename = '';

    /**
     * @var bool whether we are working in legacy mode. i.e.
     * sticking with 76 char lines and 7bit encoding where appropriate.
     */
    public bool $legacy = true;

    /**
     * @var string type of this message part, one of self::MT_*.
     * It will instruct self::render() how to produce the message output.
     */
    public string $type = '';

    /**
     * Initialise an email message part, it is recommended to use static constructors though.
     *
     * @param string $type mail part type, one of MailBase::MT_*.
     * @param $content string mail part content, if empty will attempt to load from $filename.
     * @param $filename string mail part filename that is to be used for attachments,
     * if $content not empty it will be used as the attachment name. The filename will
     * be extracted if a filepath has been supplied.
     *
     * @throws Exception
     */
    public function __construct(string $type, string $content, string $filename){
        switch($type){
            case self::MT_TEXT:
            case self::MT_HTML:
                if(!$content)
                    throw new Exception("Content has to be supplied for $type message parts.");
                $this->content = $content;
                break;
            case self::MT_INLINE:
            case self::MT_ATTACH:
                if(!$content && !$filename)
                    throw new Exception("Neither content, nor filename have been specified for an $type message part.");

                if($content && !$filename)
                    throw new Exception("Need to specify a filename for the content supplied for an $type message part.");

                /// use only filesystem safe characters for filename just to avoid any headache
                /// if the filepath has unsafe characters, preload it and supply it as $content
                if(!ctype_alnum(str_replace(['_', '.', '-', '/'], '', $filename)))
                    throw new Exception("Filename '$filename' has invalid characters.");

                if(!$content && !file_exists($filename))
                    throw new Exception("File '$filename' does not exist, can't load message part content.");

                $this->content  = $content?: file_get_contents($filename);
                $this->filename = pathinfo($filename, PATHINFO_BASENAME);
                break;
            default:
                throw new Exception("Unknown message '$type' supplied");
        }

        $this->type = $type;
    }

    /**
     * Generate a plain text email part.
     * @param string $content plain text body.
     *
     * @return MailPart
     * @throws Exception
     */
    public static function text(string $content): MailPart{
        return new MailPart(self::MT_TEXT, $content, '');
    }

    /**
     * Generate a html email part.
     * @param string $content html body.
     *
     * @return MailPart
     * @throws Exception
     */
    public static function html(string $content): MailPart{
        return new MailPart(self::MT_HTML, $content, '');
    }

    /**
     * Create an inline email attachment part.
     *
     * @param string $filename filename to download as or load content from.
     * @param string $content optional content, will use filename to load if not supplied.
     *
     * @return MailPart
     * @throws Exception
     */
    public static function inline(string $filename, string $content = ''): MailPart{
        return new MailPart(self::MT_INLINE, $content, $filename);
    }

    /**
     * Create a standard email attachment part.
     *
     * @param string $filename filename to download as or load content from.
     * @param string $content optional content, will use filename to load if not supplied.
     *
     * @return MailPart
     * @throws Exception
     */
    public static function attachment(string $filename, string $content = ''): MailPart{
        return new MailPart(self::MT_ATTACH, $content, $filename);
    }

    /**
     * Detect if non ascii chars are present.
     * @param string $text
     *
     * @return bool
     */
    public function has_8bit_chars(string $text): bool{
        return (bool)preg_match('/[\x80-\xFF]/', $text);
    }

    /**
     * Render output for a plain text message part.
     * @param string $boundary boundary to split the message by.
     *
     * @return string
     * @throws Exception
     */
    protected function render_text(string $boundary): string{
        $wide = $this->has_8bit_chars($this->content);
        $cs   = $wide? self::CS_UTF8 : self::CS_ASCII;
        $enc  = $wide? self::ENC_8BIT : self::ENC_7BIT;
        return $this->start($boundary) .
            $this->header_line(self::H_CTE, $enc) .
            $this->header_multiline(self::H_CT, self::CT_PLAIN, ['charset' => $cs]) .
            $this->nl . $this->encode($this->content, $enc);
    }

    /**
     * Render output for a html message part.
     * @param string $boundary boundary to split the message by.
     *
     * @return string
     * @throws Exception
     */
    protected function render_html(string $boundary): string{
        return $this->start($boundary) .
            $this->header_line(self::H_CTE, self::ENC_QP) .
            $this->header_multiline(self::H_CT, self::CT_HTML, ['charset' => self::CS_ASCII]) .
            $this->nl . $this->encode($this->content, self::ENC_QP);
    }

    /**
     * Render output for an inline/attachment binary message part.
     * @param string $boundary boundary to split the message by.
     *
     * @return string
     * @throws Exception
     */
    protected function render_attachment(string $boundary): string{
        $enc  = $this->legacy? self::ENC_B64 : self::ENC_BIN;
        $mime = $this->get_mime_type(pathinfo($this->filename, PATHINFO_EXTENSION));
        return $this->start($boundary) .
            $this->header_multiline(self::H_CT, $this->type, ['name' => $this->filename]) .
            $this->header_multiline(self::H_CD, $mime, ['filename' => $this->filename]) .
            $this->header_line(self::H_CTE, $enc) .
            $this->nl . $this->encode($this->content, $enc);
    }

    /**
     * Render this email part.
     * @param string $boundary boundary to split part by.
     * @return string
     * @throws Exception
     */
    public function render(string $boundary = ''): string{
        return match ($this->type) {
            self::MT_ATTACH,
            self::MT_INLINE => $this->render_attachment($boundary),
            self::MT_TEXT   => $this->render_text($boundary),
            self::MT_HTML   => $this->render_html($boundary),
        };
    }

    /**
     * Encode a string in requested format.
     * @param string $str text to encode.
     * @param string $enc one of ENC_* consts.
     *
     * @return string
     * @throws Exception
     *
     */
    public function encode(string $str, string $enc = self::ENC_B64): string{
        return match ($enc) {
            self::ENC_B64  => chunk_split(base64_encode($str), self::LEN_STD, $this->nl),
            self::ENC_7BIT => $this->wrap_body($str, self::LEN_STD),
            self::ENC_8BIT => $this->wrap_body($str, self::LEN_MAX),
            self::ENC_QP   => quoted_printable_encode($str) . $this->nl,
            default        => $str, // ENC_BIN basically
        };
    }

    /**
     * Get mime type for a file extension.
     * @param string $ext file extension.
     *
     * @return string mime type of file.
     */
    public function get_mime_type(string $ext = ''): string{
        return match ($ext) {
            'xl'                           => 'application/excel',
            'js'                           => 'application/javascript',
            'hqx'                          => 'application/mac-binhex40',
            'cpt'                          => 'application/mac-compactpro',
            'bin'                          => 'application/macbinary',
            'doc', 'word'                  => 'application/msword',
            'xlsx'                         => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'xltx'                         => 'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
            'potx'                         => 'application/vnd.openxmlformats-officedocument.presentationml.template',
            'ppsx'                         => 'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
            'pptx'                         => 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
            'sldx'                         => 'application/vnd.openxmlformats-officedocument.presentationml.slide',
            'docx'                         => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            'dotx'                         => 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
            'xlam'                         => 'application/vnd.ms-excel.addin.macroEnabled.12',
            'xlsb'                         => 'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
            'oda'                          => 'application/oda',
            'pdf'                          => 'application/pdf',
            'ai', 'eps', 'ps'              => 'application/postscript',
            'smi', 'smil'                  => 'application/smil',
            'mif'                          => 'application/vnd.mif',
            'xls'                          => 'application/vnd.ms-excel',
            'ppt'                          => 'application/vnd.ms-powerpoint',
            'wbxml'                        => 'application/vnd.wap.wbxml',
            'wmlc'                         => 'application/vnd.wap.wmlc',
            'dcr', 'dir', 'dxr'            => 'application/x-director',
            'dvi'                          => 'application/x-dvi',
            'gtar'                         => 'application/x-gtar',
            'php3', 'php4', 'php', 'phtml' => 'application/x-httpd-php',
            'phps'                         => 'application/x-httpd-php-source',
            'swf'                          => 'application/x-shockwave-flash',
            'sit'                          => 'application/x-stuffit',
            'tar', 'tgz'                   => 'application/x-tar',
            'xht', 'xhtml'                 => 'application/xhtml+xml',
            'zip'                          => 'application/zip',
            'mid', 'midi'                  => 'audio/midi',
            'mp2', 'mp3', 'mpga'           => 'audio/mpeg',
            'm4a'                          => 'audio/mp4',
            'aif', 'aifc', 'aiff'          => 'audio/x-aiff',
            'ram', 'rm'                    => 'audio/x-pn-realaudio',
            'rpm'                          => 'audio/x-pn-realaudio-plugin',
            'ra'                           => 'audio/x-realaudio',
            'wav'                          => 'audio/x-wav',
            'mka'                          => 'audio/x-matroska',
            'bmp'                          => 'image/bmp',
            'gif'                          => 'image/gif',
            'jpeg', 'jpe', 'jpg'           => 'image/jpeg',
            'png'                          => 'image/png',
            'tiff', 'tif'                  => 'image/tiff',
            'webp'                         => 'image/webp',
            'avif'                         => 'image/avif',
            'heif'                         => 'image/heif',
            'heifs'                        => 'image/heif-sequence',
            'heic'                         => 'image/heic',
            'heics'                        => 'image/heic-sequence',
            'eml'                          => 'message/rfc822',
            'css'                          => 'text/css',
            'html', 'htm', 'shtml'         => 'text/html',
            'log', 'text', 'txt'           => 'text/plain',
            'rtx'                          => 'text/richtext',
            'rtf'                          => 'text/rtf',
            'vcf', 'vcard'                 => 'text/vcard',
            'ics'                          => 'text/calendar',
            'xml', 'xsl'                   => 'text/xml',
            'wmv'                          => 'video/x-ms-wmv',
            'mpeg', 'mpe', 'mpg'           => 'video/mpeg',
            'mp4', 'm4v'                   => 'video/mp4',
            'mov', 'qt'                    => 'video/quicktime',
            'rv'                           => 'video/vnd.rn-realvideo',
            'avi'                          => 'video/x-msvideo',
            'movie'                        => 'video/x-sgi-movie',
            'webm'                         => 'video/webm',
            'mkv'                          => 'video/x-matroska',
            default                        => 'application/octet-stream',
        };
    }
}
