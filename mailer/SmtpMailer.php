<?php

namespace glowbug\mailer;

use Exception;

class SmtpMailer extends MailBase {
    /// common address list header names
    const TO = 'To';
    const CC = 'Cc';
    const BCC = 'Bcc';
    const FROM = 'From';
    const RT = 'Reply-To';  // header spelling
    const R_T = 'reply_to'; // property spelling

    /**
     * Email priority.
     * Options: 0 (default, not set), 1 = High, 3 = Normal, 5 = low.
     * When 0, the header is not set at all.
     *
     * @var int
     */
    public int $priority = 0;

    /**
     * The envelope sender of the message.
     * This will usually be turned into a Return-Path header by the receiver,
     * and is the address that bounces will be sent to.
     * If not empty, will be passed as the 'MAIL FROM' value over SMTP.
     *
     * @var string
     */
    public string $sender = '';

    /**
     * Subject header of the message.
     *
     * @var string
     */
    public string $subject = '';

    /**
     * An HTML or plain text message body.
     * If HTML then call set_html(true).
     *
     * @var string
     */
    public string $html_body = '';

    /**
     * The plain-text message body.
     * This body can be read by mail clients that do not have HTML email
     * capability such as mutt & Eudora.
     * Clients that can read HTML will view the normal Body.
     *
     * @var string
     */
    public string $text_body = '';

    /**
     * The hostname to use in the Message-ID header and as default HELO string.
     * Will default to gethostname() return value if not set.
     *
     * @see SmtpMailer::$smtp_helo
     *
     * @var string
     */
    public string $hostname = '';

    /**
     * @var string Message-ID header  in the format "<id@domain>", if empty a random one will be generated using $hostname.
     */
    public string $message_id = '';

    /**
     * @var string message Date to be used in the Date header, new one will be generated if left empty.
     */
    public string $date = '';

    /**
     * @var ?SMTP
     */
    protected ?SMTP $smtp = null;

    /**
     * Get an instance to use for SMTP operations.
     * Override this function to load your own SMTP implementation,
     * or set one with setSMTPInstance.
     *
     * @return SMTP
     */
    public function get_smtp(): SMTP{
        if(!$this->smtp)
            $this->smtp = new SMTP();
        return $this->smtp;
    }

    /**
     * Close the active SMTP session if one exists.
     */
    public function smtp_close(){
        if($this->smtp && $this->smtp->connected()){
            $this->smtp->quit();
            $this->smtp->close();
        }
    }

    /**
     * @var string smtp hostname.
     */
    public string $smtp_host = 'localhost';

    /**
     * @var int smtp server port.
     */
    public int $smtp_port = 25;

    /**
     * @var array additional smtp options, can be left empty.
     */
    public array $smtp_options = [];

    /**
     * @var string HELO/EHLO name used for SMTP connection, $hostname will be used if left empty.
     */
    public string $smtp_helo = '';

    /**
     * @var string smtp server username.
     */
    public string $smtp_user = '';

    /**
     * @var string smtp server password.
     */
    public string $smtp_pass = '';

    /**
     * @var int default smtp timeout.
     */
    public int $smtp_timeout = 30;

    /**
     * Comma separated list of DSN notifications
     * 'NEVER' under no circumstances a DSN must be returned to the sender.
     *         If you use NEVER all other notifications will be ignored.
     * 'SUCCESS' will notify you when your mail has arrived at its destination.
     * 'FAILURE' will arrive if an error occurred during delivery.
     * 'DELAY'   will notify you if there is an unusual delay in delivery, but the actual
     *           delivery's outcome (success or failure) is not yet decided.
     *
     * @see https://tools.ietf.org/html/rfc3461 See section 4.1 for more information about NOTIFY
     */
    public string $smtp_dsn = '';

    /**
     * @var bool Whether to keep the SMTP connection open after each message when a bunch needs to be sent out.
     */
    public bool $smtp_keepalive = false;

    /***
     * @var bool whether to generate VERP addresses on send.
     * @see https://en.wikipedia.org/wiki/Variable_envelope_return_path
     */
    public bool $smtp_do_verp = false;

    /**
     * @var string X-Mailer header value.
     */
    public string $x_mailer = 'Glowbug Mailer v0.1';

    /**
     * @var array From header values for the message in form of
     * a single element assoc array [$address => $name].
     */
    protected array $From = [];

    /**
     * Set the From field, will also set the sender field.
     * @param string $address
     * @param string $name recipient name.
     *
     * @return bool
     * @throws Exception
     *
     */
    public function set_from(string $address, string $name = ''): bool{
        $address      = $this->validate_address($address);
        $this->sender = $address;

        $this->From[$address] = $name;
        return true;
    }

    /**
     * Get address of the sender.
     * @return ?string
     */
    public function from_address(): ?string{
        return array_key_first($this->From);
    }

    /**
     * Get name of the sender.
     * @return ?string
     */
    public function from_name(): ?string{
        return $this->From[$this->from_address()];
    }

    /**
     * @var array list of 'To' names and addresses in form of $address => $name.
     */
    protected array $To = [];

    /**
     * @var array list of 'Cc' names and addresses in form of $address => $name.
     */
    protected array $Cc = [];

    /**
     * @var array list of 'Bcc' names and addresses in form of $address => $name.
     */
    protected array $Bcc = [];

    /**
     * @var array list of reply-to names and addresses in form of $address => $name.
     */
    protected array $reply_to = [];

    /**
     * List of all kinds of addresses in form of $address => $name.
     * Includes all of $to, $cc, $bcc.
     *
     * @return array
     */
    public function all_recipients(): array{
        return array_merge($this->To, $this->Cc, $this->Bcc);
    }

    /**
     * @var MailPart[] attachment list.
     */
    protected array $attachments = [];

    /**
     * Add an attachment to the list.
     * @param MailPart $part attachment to add.
     * @return MailPart
     */
    public function add_attachment(MailPart $part): MailPart{
        return $this->attachments[] = $part;
    }

    public function __construct($hostname = ''){
        if(!$this->hostname = $hostname?: gethostname())
            throw new Exception("Could not establish this machine's hostname.");

        $uid              = $this->uid();
        $this->message_id = "<$uid@$this->hostname>";
    }

    /**
     * Destructor.
     */
    public function __destruct(){
        $this->smtp_close();
    }

    /**
     * Check that a string looks like an email address. Will return the
     * address lowercase on success, or throw an exception on failure.
     *
     * @param string $address The email address to check
     * @return string
     * @throws Exception
     */
    public function validate_address(string $address): string{
        if(!filter_var($address, FILTER_VALIDATE_EMAIL))
            throw new Exception("Invalid email address '$address'.");
        return strtolower($address);
    }

    /**
     * Add an address to one of the recipient arrays or to the Reply-To array.
     * Addresses that have been added already return false, but do not throw exceptions.
     *
     * @param string $type one of 'To', 'Cc', 'Bcc', or 'reply_to'
     * (there are consts for that).
     * @param string $address the email address to use.
     * @param string $name name of the recipient.
     *
     * @return bool true on success, false if address already used.
     * @throws Exception when the email address is malformed.
     *
     */
    protected function add_address_type(string $type, string $address, string $name = ''): bool{
        $address = $this->validate_address($address);
        if(!isset($this->{$type}[$address])){
            $this->{$type}[$address] = $name;
            return true;
        }
        return false;
    }

    /**
     * Add a "To" address.
     * @param string $address The email address to send to
     * @param string $name recipient name.
     *
     * @return bool true on success, false if address already added.
     * @throws Exception
     *
     */
    public function add_to(string $address, string $name = ''): bool{
        return $this->add_address_type(self::TO, $address, $name);
    }

    /**
     * Add a "Cc" address.
     * @param string $address The email address to send to
     * @param string $name recipient name.
     *
     * @return bool true on success, false if address already added.
     * @throws Exception when email address is malformed.
     *
     */
    public function add_cc(string $address, string $name = ''): bool{
        return $this->add_address_type(self::CC, $address, $name);
    }

    /**
     * Add a "Bcc" address.
     * @param string $address The email address to send to
     * @param string $name recipient name.
     *
     * @return bool true on success, false if address already added.
     * @throws Exception when email address is malformed.
     *
     */
    public function add_bcc(string $address, string $name = ''): bool{
        return $this->add_address_type(self::BCC, $address, $name);
    }

    /**
     * Add a "Reply-To" address.
     * @param string $address The email address to reply to
     * @param string $name recipient name.
     *
     * @return bool true on success, false if address already added.
     * @throws Exception when email address is malformed.
     *
     */
    public function add_rt(string $address, string $name = ''): bool{
        return $this->add_address_type(self::R_T, $address, $name);
    }

    /**
     * Generate root message content type.
     * @return string
     */
    public function message_type(): string{
        if($this->text_body && $this->html_body)
            return $this->attachments? self::CT_MIXED : self::CT_ALT;

        if($this->attachments)
            if($this->text_body or $this->html_body)
                return self::CT_MIXED;

        if($this->text_body)
            return self::CT_PLAIN;

        return self::CT_HTML;
    }

    /**
     * Format an address for use in a message header.
     * @param $address string email address.
     * @param string $name recipient name.
     *
     * @return string
     */
    public function format_address(string $address, string $name = ''): string{
        return $name? "$name <$address>" : $address;
    }

    /**
     * @param string $ct
     * @param string $boundary
     * @param MailPart[] $parts
     * @return string
     * @throws Exception
     */
    public function wrap_part(string $boundary, array $parts, string $ct): string{
        if(!$parts)
            return '';
        static $l = 1.;
        $bound = "l" . $l++ . "-" . $this->uid();
        $parts = array_map(fn($p) => $p->render($bound), $parts);
        return $this->start($boundary) .
            $this->boundary_header($ct, $bound) .
            implode('', $parts) .
            $this->end($bound);
    }

    /**
     * Generate text copy from both plain and html parts.
     * @param $bound string boundary.
     * @param bool $root overall message has just one Mailer::MT_ALT part.
     * @return string will return empty string if no copy present.
     * @throws Exception
     */
    public function text_part(string $bound = '', bool $root = false): string{
        $copy = [];
        if($this->text_body)
            $copy[] = MailPart::text($this->text_body);
        if($this->html_body)
            $copy[] = MailPart::html($this->html_body);
        if(!$copy)
            return '';

        $count = count($copy);
        if($count == 1)
            return $copy[0]->render($bound);

        if($count == 2 && $root)
            return $copy[0]->render($bound) .
                $copy[1]->render($bound) .
                $this->end($bound);

        return $this->wrap_part($bound, $copy, self::CT_ALT);
    }

    /**
     * Render message.
     * @return string root message headers output.
     * @throws Exception
     */
    public function make_message(){
        $lines = []; // header collector

        /// make recipients header function
        $mrh = function($type, $addresses = null) use (&$lines){
            if(!$list = $addresses ?? $this->{$type})
                return;

            $addresses = [];
            foreach($list as $address => $name)
                $addresses[] = $this->format_address($address, $name);

            $lines[] = $this->header_line($type, implode(', ', $addresses));
        };

        $mrh(self::FROM);
        $mrh(self::TO);
        $mrh(self::CC);
        $mrh(self::BCC);
        $mrh(self::RT, $this->reply_to);

        /// add header only if it has value
        $add = function($label, $val) use (&$lines){
            if($val)
                $lines[] = $this->header_line($label, $val);
        };

        $add('Message-ID', $this->message_id);
        $add('X-Priority', $this->priority);
        $add('Date', $this->date?: $this->date());
        $add('Subject', $this->subject);
        $add('X-Mailer', $this->x_mailer);
        $add('Mime-Version', "1.0");

        $bound = "root-" . $this->uid();
        switch($type = $this->message_type()){
            case self::CT_ALT:
                $lines[] = $this->boundary_header($type, $bound);
                $lines[] = $this->text_part($bound, true);
                break;

            case self::CT_MIXED:
                $lines[] = $this->boundary_header($type, $bound);
                $lines[] = $this->text_part($bound);
                $lines[] = $this->wrap_part($bound, $this->attachments, self::CT_MIXED);
                $lines[] = $this->end($bound);
                break;

            case self::CT_PLAIN:
            case self::CT_HTML:
            default:
                $lines[] = $this->text_part();
        }

        return implode('', $lines);
    }

    /**
     * Initiate a connection to an SMTP server.
     * @return SMTP
     * @throws Exception
     */
    public function smtp_connect(): SMTP{
        $smtp = $this->get_smtp();
        if($smtp->connected())
            return $smtp;

        $smtp->setVerp($this->smtp_do_verp);
        $smtp->setTimeout($this->smtp_timeout);

        /// check if openSSL ext present
        if(!defined('OPENSSL_ALGO_SHA256'))
            throw new Exception("OpenSSL extension not enabled.");

        if($smtp->connect($this->smtp_host, $this->smtp_port, $this->smtp_timeout, $this->smtp_options)){
            try {
                $hello = $this->smtp_helo?: $this->hostname;
                $smtp->hello($hello);

                if($smtp->getServerExt('STARTTLS')){
                    if(!$smtp->startTLS())
                        throw new Exception("Error when initiating TLS connection with SMTP host.");
                    else $smtp->hello($hello); // need to resend hello after TLS handshake
                }

                if(!$smtp->authenticate($this->smtp_user, $this->smtp_pass))
                    throw new Exception("Could not authenticate with SMTP host.");

                return $smtp;
            } catch(Exception $ex) {
                $this->smtp_close();
                throw $ex;
            }
        }
        throw new Exception("Could not connect to smtp server $this->smtp_host:$this->smtp_port fsr.");
    }

    /**
     * Create a message and send it.
     * @return array [$message, $bad_recipients, $good_recipients, $smtp->getLastTransactionID]
     *
     * @throws Exception exceptions thrown when there no recipients, or there are
     * issues when connecting and interacting with the smtp host.
     *
     */
    public function smtp_send(): array{
        if(!$all = $this->all_recipients())
            throw new Exception("No addresses to send to provided.");

        $smtp = $this->smtp_connect();
        $err  = fn($prefix = '') => "$prefix'" . $smtp->getError()['detail'] . "'";
        if(!$smtp->mail_from($this->sender))
            throw new Exception($err("'MAIL FROM' error: "));

        $bad_recipients  = [];
        $good_recipients = [];
        foreach($all as $address => $name)
            if(!$smtp->recipient($address, $this->smtp_dsn))
                $bad_recipients[$address] = $err("Recipient error: ");
            else $good_recipients[] = $address;

        $message = $this->make_message();
        if(!$smtp->data($message))
            throw new Exception($err("SMTP host error: "));

        if($this->smtp_keepalive)
            $smtp->reset();
        else
            $this->smtp_close();

        return [$message, $bad_recipients, $good_recipients, $smtp->getLastTransactionID()];
    }

    /**
     * Clear all recipient types.
     */
    public function clear_all_recipients(){
        $this->To  = [];
        $this->Cc  = [];
        $this->Bcc = [];
    }

    /**
     * Clear all filesystem, string, and binary attachments.
     */
    public function clear_attachments(){
        $this->attachments = [];
    }

    /**
     * Return an RFC 822 formatted date.
     * @return string
     */
    public function date(): string{
        date_default_timezone_set(@date_default_timezone_get());
        return date('r');
    }
}
