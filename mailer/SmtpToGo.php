<?php

namespace glowbug\mailer;

use Exception;
use glowbug\misc\Sender;

class SmtpToGo {
    /**
     * @var string SMTP2GO api key to use in interaction with the service.
     */
    protected $api_key = 'api-6AF423E660D511EBA2BFF23C91BBF4A0';

    public $base_url = "https://api.smtp2go.com/v3";

    public $attachments = [];

    /**
     * @var string from email address.
     */
    public $from;

    /**
     * @var Sender
     */
    private $sender;

    /**
     * Add attachment to the list.
     * @param $filename string file name to send as.
     * @param $string string content of the attachment.
     * @return array
     */
    public function add_attachment($filename, $string){
        $type = pathinfo($filename, PATHINFO_EXTENSION);
        return $this->attachments[] = [
            "filename" => $filename,
            "fileblob" => base64_encode($string),
            'mimetype' => "application/$type",
        ];
    }

    /**
     * @param $from string from address.
     * @param $api_key string optional SMTP2GO api key.
     */
    public function __construct($from, $api_key = ''){
        $this->from = $from;
        if($api_key)
            $this->api_key = $api_key;
        $this->sender = new Sender(false, true);
    }

    /**
     * Send email message.
     * @param $address string email address to send to.
     * @param $subject string
     * @param $body string text body.
     * @return array|false
     * @throws Exception
     */
    public function send($address, $subject, $body){
        $data = [
            'api_key'     => $this->api_key,
            'to'          => [$address],
            'sender'      => $this->from,
            'subject'     => $subject,
            'text_body'   => $body,
            'attachments' => $this->attachments,
        ];
        return $this->sender->send_post_json("$this->base_url/email/send", $data);
    }
}
