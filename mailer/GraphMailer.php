<?php

namespace glowbug\mailer;

use Exception;
use glowbug\misc\Graph;

/**
 * Class for dealing with MS Office 365 mail forwarding as their
 * SMTP communication is impossible to configure/get to work.
 */
class GraphMailer extends Graph {

    const name = 'name';
    const address = 'address';
    const content = 'content';
    const content_type = 'contentType';
    const email_address = 'emailAddress';

    /// outlook mail field consts
    const id = 'id';
    const sent_on = 'sentDateTime';
    const subject = 'subject';
    const preview = 'bodyPreview';
    const importance = 'importance';
    const convo_id = 'conversationId';
    const is_read = 'isRead';
    const body = 'body';
    const sender = 'sender';
    const tos = 'toRecipients';
    const ccs = 'ccRecipients';
    const tos_basic = 'toRecipientsBasic';
    const ccs_basic = 'ccRecipientsBasic';
    const reply_to = 'replyTo';
    const attachments = 'attachments';
    const images = 'images';
    const content_bytes = 'contentBytes';
    const is_inline = 'isInline';

    /// outlook mail attachment consts
    /// id, contentType, name, isInline, contentBytes are reused
    const data_type = '@odata.type';
    const media_type = '@odata.mediaContentType';
    const modified_at = 'lastModifiedDateTime';
    const size = 'size';
    const content_id = 'contentId';
    const content_location = 'contentLocation';

    /**
     * Create a basic attachment for Outlook.
     * @param $filename string file name of the attachment.
     * @param $string string content of the attachment.
     * @return array
     */
    public static function make_attachment($filename, $string){
        $type = pathinfo($filename, PATHINFO_EXTENSION);
        return [
            self::data_type     => "#microsoft.graph.fileAttachment",
            self::name          => $filename,
            self::content_type  => "application/$type",
            self::size          => strlen($string),
            self::content_bytes => base64_encode($string),
        ];
    }

    /**
     * Get messages of a user, can be convenient for detecting responses
     * to communication sent out by a server end app.
     * @param $user_id string MS user id.
     * @return array
     * @throws Exception
     */
    public function get_messages($user_id){
        $userReq  = "users/$user_id";
        $messages = $this->get("$userReq/mailFolders/Inbox/Messages");

        if(isset($messages['error'])){
            $error = $messages['error'];
            throw new Exception("$error[code]  $error[message]");
        }

        $messageList = [];
        /// extract email address from a recipients' list entry
        $getAddress = fn($addr) => $addr[self::email_address][self::address];
        foreach($messages['value'] as $message){
            $attachments = $this->get("$userReq/messages/$message[id]/attachments")['value'];

            foreach($attachments as &$attachment)
                if($attachment[self::data_type] == '#microsoft.graph.referenceAttachment')
                    $attachment[self::content_bytes] = base64_encode('This is a link to a SharePoint online file, not yet supported');

            $basic         = fn($field) => array_map($getAddress, $message[$field]);
            $messageList[] = [
                self::id          => $message[self::id],
                self::sent_on     => $message[self::sent_on],
                self::subject     => $message[self::subject],
                self::preview     => $message[self::preview],
                self::importance  => $message[self::importance],
                self::convo_id    => $message[self::convo_id],
                self::is_read     => $message[self::is_read],
                self::body        => $message[self::body],
                self::sender      => $message[self::sender],
                self::tos         => $message[self::tos],
                self::ccs         => $message[self::ccs],
                self::tos_basic   => $basic(self::tos),
                self::ccs_basic   => $basic(self::ccs),
                self::reply_to    => $message[self::reply_to],
                self::attachments => $attachments
            ];
        }
        return $messageList;
    }

    /**
     * Delete email message from user's account.
     * @param $user_id string AD user id.
     * @param $message_id string message id in AD format.
     * @param $move_to_trash bool whether to move to trash, or delete completely.
     * @return void
     * @throws Exception
     */
    public function delete_message($user_id, $message_id, $move_to_trash = true){
        if($move_to_trash)
            $this->post("/users/$user_id/messages/$message_id/move", ["destinationId" => "deleteditems"]);
        else
            $this->delete("/users/$user_id/messages/$message_id");
    }

    /**
     * Send an email message on behalf of an AD user.
     * @param $user_id string AD user id.
     * @param $to_email string|string[] email address to send to.
     * @param $subject string subject line.
     * @param $body string body of the message.
     * @param $attachments array
     * @return null
     * @throws Exception
     */
    public function send_mail($user_id, $to_email, $subject, $body, $attachments = []){
        if(is_string($to_email))
            $to_email = [$to_email];

        $makeEmail = fn($email) => [
            self::email_address => [
                self::name    => '',
                self::address => $email,
            ],
        ];

        $message = [
            self::tos     => array_map($makeEmail, $to_email),
            self::subject => $subject,
            self::body    => [
                self::content      => $body,
                self::content_type => 'text',
            ],
            # self::convo_id => $conv_id,
            # self::images      => [],
            # self::importance     => $importance,
        ];
        if($attachments)
            $message[self::attachments] = $attachments;

        return $this->post("/users/$user_id/sendMail", [
            'saveToSentItems' => true,
            'message'         => $message,
        ]);
    }
}
