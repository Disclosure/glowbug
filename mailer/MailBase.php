<?php

namespace glowbug\mailer;

use Exception;

abstract class MailBase {
    /// charset types
    const CS_ASCII = 'us-ascii';
    const CS_UTF8 = 'utf-8';

    /// root message content types
    const CT_HTML = 'text/html';
    const CT_PLAIN = 'text/plain';
    const CT_MIXED = 'multipart/mixed';
    const CT_ALT = 'multipart/alternative';

    /// common header types
    const H_CT = 'Content-Type';
    const H_CD = 'Content-Disposition';
    const H_CTE = 'Content-Transfer-Encoding';

    /// message part types
    const MT_TEXT = 'text';
    const MT_HTML = 'html';
    const MT_INLINE = 'inline';
    const MT_ATTACH = 'attachment';

    const CRLF = "\r\n";

    /**
     * @var string new line shortcut for string interpolations.
     */
    protected string $nl = self::CRLF;

    /**
     * @var int 7bit message max line length including line breaks.
     */
    const LEN_STD = 80;

    /**
     * @var int 8bit message max line length including line breaks.
     */
    const LEN_MAX = 1000;

    /**
     * Generate a random uid, typically to be used for boundaries.
     * @return string
     * @throws Exception
     */
    public function uid(): string{
        return md5(random_bytes(128));
    }

    /**
     * Conditionally output two new lines in a row.
     * @param $double_nl bool 1 new line if false, 2 otherwise.
     * @return string
     */
    protected function dnl(bool $double_nl): string{
        return $double_nl? "$this->nl$this->nl" : $this->nl;
    }

    /**
     * Create a message part start boundary.
     * @param string $boundary boundary to use, will emit empty string
     * if no boundary has been supplied.
     * @return string
     */
    protected function start(string $boundary = ''): string{
        return $boundary? "--$boundary$this->nl" : '';
    }

    /**
     * Create a message part end boundary.
     * @param string $boundary boundary to use.
     * @return string
     */
    protected function end(string $boundary){
        return "--$boundary--$this->nl$this->nl";
    }

    /**
     * Replace bare new lines and wrap text to a specific line length.
     * @param $string string input text.
     * @param string $maxLineLen longest line allowed (not including CRLF at the end).
     * @param string $nl new line separator.
     * @param bool $appendNl whether to append line break at the end.
     *
     * @return string
     */
    public function wrap_text(string $string, string $maxLineLen, string $nl, bool $appendNl = false): string{
        $lines    = [];
        $prevChar = '';
        $max = strlen($string);
        $maxLineLen -= strlen($nl); // allow for length of newline string
        $lineLen  = $lineStart = $lastSpace = 0;
        for($pos = 0; $pos < $max; $pos++){
            $lineLen = $pos - $lineStart;
            if($lineLen > $maxLineLen){
                $lines[]   = substr($string, $lineStart, $lastSpace - $lineStart);
                $lineStart = $lastSpace + 1;
                $lineLen   = 0;
            }

            $char = $string[$pos];
            if($char == "\n"){
                $isR       = $prevChar == "\r";
                $span      = $lineLen - ($isR? 1 : 0);
                $lines[]   = substr($string, $lineStart, $span);
                $lineStart = $pos + 1;
                $lineLen   = 0;
            } elseif($char == ' ')
                $lastSpace = $pos;

            $prevChar = $char;
        }
        if($lineLen)
            $lines[] = substr($string, $lineStart);
        if($appendNl)
            $lines[] = '';

        return implode($nl, $lines);
    }

    /**
     * Wrap a header line.
     * @param $string string full header.
     * @return string
     */
    public function fold($string){
        /// breaks are followed by space, need to reduce max line len
        return $this->wrap_text($string, self::LEN_STD - 1, "\r\n ");
    }

    /**
     * Wrap body message, only to be used on plain text.
     * Will append a new line at the end if not present.
     * @param $string string copy.
     * @param $len int maximum line length.
     * @return string
     */
    public function wrap_body($string, $len){
        $maxLen = strlen($string);
        $addNl  = !($string[$maxLen - 2] == "\r" && $string[$maxLen - 1] == "\n");
        return $this->wrap_text($string, $len, $this->nl, $addNl);
    }

    public function has_utf($string){
        return mb_detect_encoding($string, null, true) == "UTF-8";
    }

    /**
     * Format a header line.
     *
     * @param string $name header name.
     * @param string $value header value.
     * @param bool $dnl whether to append double new line at the end.
     *
     * @return string
     */
    protected function header_line(string $name, string $value, bool $dnl = false): string{
        $value = str_replace(["\r", "\n"], ' ', $value);
        if($this->has_utf($value))
            return "$name: =?utf-8?Q?=$this->nl" . quoted_printable_encode($value) . $this->dnl($dnl);
        return $this->fold("$name: $value") . $this->dnl($dnl);
    }

    /**
     * Create a multiline header similar to
     *
     * Content-Type: text/html;
     *     charset=us-ascii
     *
     * Some subheader values will be wrapped in quotes.
     *
     * @param $name string header name, like 'Content-Type'.
     * @param $value string header value, like 'text/html'.
     * @param $sublines array assoc array of header sublines, like ['charset'=>'us-ascii'].
     * @param bool $double_nl
     *
     * @return string
     */
    protected function header_multiline(string $name, string $value, array $sublines = [], bool $double_nl = false): string{
        $lines = [];
        $main  = $this->header_line($name, "$value;");
        foreach($sublines as $key => $val){
            $quote   = $key == "name"? "\"$val\"" : $val;
            $lines[] = " $key=$quote";
        }
        return $main . implode(";$this->nl", $lines) . $this->dnl($double_nl);
    }

    public function boundary_header($ctype, $bound){
        return $this->header_multiline(self::H_CT, $ctype,
            ['boundary' => $bound], true);
    }
}
