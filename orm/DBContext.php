<?php

namespace glowbug\orm;
use Exception, PDO, PDOStatement, ReflectionClass, ReflectionException, ReflectionProperty;

class DBContext extends PDO {

    const MYSQL = 'mysql';
    const SQLSRV = 'sqlsrv';
    const SQLITE = 'sqlite';

    /**
     * @var string DB driver type.
     */
    public $driver;

    public function is_sqlite(){
        return $this->driver == self::SQLITE;
    }

    public function is_mysql(){
        return $this->driver == self::MYSQL;
    }

    /**
     * Creates a new DB context instance that is used to operate on data in object from.
     * @param string $driver one of "mysql", "sqlsrv" or "sqlite"
     * @param string $host
     * @param string $dbname
     * @param string $username
     * @param string $password
     * @param bool $create_if_not_exists in mysql only, will create a new DB
     * with a name given if it does not exist on the server.
     * @throws Exception
     */
    public function __construct($driver, $host, $dbname, $username, $password, $create_if_not_exists = false){
        switch ($driver) {
            case self::MYSQL:
                try {
                    parent::__construct("mysql:host=$host; dbname=$dbname", $username, $password);
                } catch (Exception $ex) {
                    if ($ex->getCode() == 1049 && $create_if_not_exists) {
                        parent::__construct("mysql:host=$host", $username, $password);
                        $this->execute("create database $dbname; use $dbname", []);
                    } else throw $ex;
                }
                break;
            case self::SQLSRV:
                parent::__construct("sqlsrv:server=$host;database=$dbname", $username, $password);
                break;
            case self::SQLITE:
                parent::__construct("sqlite:$dbname");
                break;
            default:
                throw new Exception("Database driver '$driver' unknown");
        }

        $this->driver = $driver;
        $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    /**
     * Execute an SQL statement in the current DB context.
     * @param string $query
     * @param array $params a named $param=>$value array of SQL params.
     * @return bool
     */
    public function execute($query, $params){
        $prep = $this->prepare($query);
        return $prep->execute($params);
    }

    /**
     * Execute an SQL query. Load is used since query overrides PDO's query().
     * @param string $query
     * @param array $params a named $param=>$value array of SQL params.
     * @return PDOStatement
     */
    public function load($query, $params = []){
        $prep = $this->prepare($query);
        $prep->execute($params);
        return $prep;
    }

    /**
     * Run an insert query. Exactly the same as execute() only also checks
     * for the last inserted ID in the table.
     * @param $query
     * @param $params
     * @return string the ID of the inserted table row
     */
    public function insert($query, $params){
        $prep = $this->prepare($query);
        $this->beginTransaction();
        $prep->execute($params);
        $id = $this->lastInsertId();
        $this->commit();
        return $id;
    }

    /**
     * Returns a single column value resulting from an SQL load.
     * @param string $query
     * @param array $params a named $param=>$value array of SQL params.
     * @return mixed
     */
    public function field_row($query, $params = []){
        $prep = $this->load($query, $params);
        return $prep->fetch(PDO::FETCH_COLUMN);
    }

    /**
     * Returns all values for a given column from an SQL load.
     * @param string $query
     * @param array $params a named $param=>$value array of SQL params.
     * @return array a numeric array of the column values.
     */
    public function field_rows($query, $params = []){
        $prep = $this->load($query, $params);
        return $prep->fetchAll(PDO::FETCH_COLUMN);
    }

    /**
     * Return a single row result from an SQL load.
     * @param string $query
     * @param array $params a named $param=>$value array of SQL params.
     * @return array an assoc array of row values for a DB row.
     */
    public function fields_row($query, $params = []){
        $prep = $this->load($query, $params);
        return $prep->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Return a multiple rows result from an SQL load.
     * @param string $query
     * @param array $params a named $param=>$value array of SQL params.
     * @return array an assoc array of row values for a DB row.
     */
    public function fields_rows($query, $params = []){
        $prep = $this->load($query, $params);
        return $prep->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Returns all matching model objects from DB for a give SQL load.
     * @param string $query
     * @param string $class name of the model class to be fetched.
     * @param array $params a named $param=>$value array of SQL params. Supply null if not needed.
     * @param array $args object constructor arguments
     * @return array of objects.
     */
    public function objects($query, $class, $params = [], $args = []){
        $prep = $this->load($query, $params);
        return $prep->fetchAll(PDO::FETCH_CLASS, $class, $args);
    }

    /**
     * Returns a single model object from DB for a given SQL load.
     * @param string $query
     * @param string $class name of the model class to be fetched.
     * @param array $params a named $param=>$value array of SQL params.
     * @param array $args object constructor arguments
     * @return mixed
     */
    public function object($query, $class, $params = [], $args = []){
        $prep   = $this->load($query, $params);
        $result = $prep->fetchObject($class, $args);
        return $result ? $result : null;
    }

    /**
     * Check if table with a specific name exits in DB.
     * @param string $table table name
     * @return mixed
     */
    public function table_exists($table){
        $query = "SHOW TABLES LIKE '$table';";
        return $this->field_row($query);
    }

    /**
     * Get the list of this DB context's Tables.
     * @return Table[]
     */
    public function list_tables(){
        $refClass = new ReflectionClass($this);
        $props    = $refClass->getProperties(ReflectionProperty::IS_PUBLIC);
        $fields   = [];
        foreach ($props as $prop) {
            $field = $this->{$prop->name};
            if (gettype($field) != 'object')
                continue;
            if (get_class($field) == 'glowbug\\orm\\Table')
                $fields[] = $field;
        }
        return $fields;
    }

    /**
     * Generate a statement to drop all tables from the DB.
     * @param false $run whether to run the statements.
     * @return string
     */
    public function drop_tables($run = false){
        $tables = $this->list_tables();
        $drops  = array_map(function ($t) use ($run){
            return $t->drop_db_table($run);
        }, $tables);
        return implode("\n", $drops);
    }

    /**
     * Make query to create DB tables of this context.
     * @param bool $run whether to run the resulting statements.
     * @return string
     * @throws ReflectionException
     */
    public function create_tables($run = false){
        $tables = $this->list_tables();
        $create = array_map(function ($t) use ($run){
            return $t->create_db_table($run);
        }, $tables);
        return implode("\n", $create) . PHP_EOL;
    }

    /**
     * @param false $run whether to run the resulting statements.
     * @return string
     * @throws ReflectionException
     */
    public function recreate_tables($run = false){
        $drops   = $this->drop_tables($run);
        $creates = $this->create_tables($run);
        return "$drops\n\n$creates";
    }

    /**
     * Make a query to create a DB and its tables.
     * @param string $name name of the DB, not required for sqlite.
     * @param false $run whether to run the resulting statement.
     * @return string
     * @throws ReflectionException
     */
    public function create_db($name = '', $run = false){
        $sqlite = $this->is_sqlite();
        if (!$name && !$sqlite)
            throw new Exception("DB name can be only omitted for sqlite DB drivers.");
        $create = !$sqlite ? "CREATE DATABASE `$name`;\n" : '';
        $tables = $this->create_tables($run);
        $query  = $create . $tables;
        if ($run)
            $this->exec($query);
        return $query;
    }

    /**
     * Describe a DB table and optionally convert it to a PHP class.
     * Handy when need to extract models from an existing database.
     * @param $table_name string name of the table to describe.
     * @param string $to_class name of the PHP class to produce.
     * @return array|string
     */
    public function describe($table_name, $to_class = ''){
        $command   = $this->is_mysql() ? 'describe' : 'sp_columns';
        $structure = $this->load("$command $table_name")->fetchAll(PDO::FETCH_ASSOC);
        if (!$to_class)
            return $structure;

        $printMysql = function ($field){
            $typeName = $field['Type'];
            $type     = strpos($typeName, 'int') === 0 ? 'int' : 'string';
            $canNull  = $field['Null'] == 'YES';
            $null     = $canNull ? 'NULL' : 'NOT NULL';
            $default  = $field['Default'] ? "default $field[Default]" : '';
            if ($canNull)
                $type = "?$type";
            return [
                $field['Field'],
                $type,
                $typeName,
                $null,
                $default,
                $field['Extra']
            ];
        };

        $printTsql = function ($field){
            $typeName = $field['TYPE_NAME'];
            $type     = substr($typeName, 0, 3);
            $type     = in_array($type, [
                'int',
                'bit'
            ]) ? 'int' : 'string';
            $canNull  = $field['NULLABLE'];
            $null     = $canNull ? 'NULL' : 'NOT NULL';
            $default  = $field['COLUMN_DEF'] ? "default $field[COLUMN_DEF]" : '';
            if ($canNull)
                $type = "?$type";
            return [
                $field['COLUMN_NAME'],
                $type,
                "$typeName($field[LENGTH])",
                $null,
                $default,
                ''
            ];
        };

        $fields   = [];
        $callback = $this->is_mysql() ? $printMysql : $printTsql;
        foreach ($structure as $field) {
            list($name, $type, $typeName, $null, $default, $extra) = $callback($field);
            $fields[] = <<< text
    /**
     * @var $type
     * @$this->driver $typeName $null $default $extra
     */
     public $type \$$name;
text;
        }

        return "<?php\n\nclass $to_class {\n" . implode("\n\n", $fields) . "\n}";
    }

    /**
     * Get an array of public properties of a supplied object.
     * @param $object Model
     * @return string[]
     */
    static public function object_fields($object){
        return get_object_vars($object);
    }

    /**
     * Get an array of public properties of a class.
     * @param $class string name of the class.
     * @param bool $keys whether to return field names.
     * @return string[]
     */
    static public function class_fields($class, $keys = true){
        $fields = get_class_vars($class);
        if ($keys)
            return array_keys($fields);
        return $fields;
    }
}
