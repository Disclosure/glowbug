<?php

namespace glowbug\orm;

use dbs\DB;
use Exception;
use LogicException;

/**
 * Instances of Model subclasses are commonly used for data mapping
 * to and from the DB context via Table class instances. Not really
 * compulsory to use it, but it has a few routines that make it easy
 * to bounce changes and new records to the DB using add() and update().
 * Requires a DBContext instance to work, which can be shared with the
 * Table instance that performs mapping between the DB and model class.
 */
abstract class Model {

    /**
     * @var DB context model belongs to. Has to remain protected so it
     * does not get in the way when sending the model in and out DB.
     */
    protected $db;

    /**
     * Get the DB instance (to work around protected status).
     * @return DB
     */
    public function get_db(){
        return $this->db;
    }

    /**
     * @var Table in DB this object is mapping to.
     */
    protected $table;

    /**
     * @var array model field names that contain json data.
     */
    static protected $json_fields = [];

    /**
     * Return this class's json fields, not sure it is necessary
     * to have an accessor,todo: calls similar to User::json_fields might work.
     * @return array get names of the model fields that contain json data.
     */
    public static function json_fields(){
        return static::$json_fields;
    }

    /**
     * Creates a new abstract DB model class.
     * @param $table Table mapped table name
     */
    public function __construct($table){
        if(!$table)
            throw new Exception("Model table hasn't been supplied.");
        $this->table = $table;
        $this->db    = $table->db;
    }

    /**
     * Make a ->w() argument with this models foreign key name and
     * its respective primary key value.
     * @param bool $qualified whether to generate the name fully
     * qualified, i.e. table_name.foreign_key
     * @return array
     */
    public function fkey_id($qualified = false){
        $fkey = $this->table->foreign_key;
        if($qualified)
            $fkey = "{$this->table->name}.$fkey";
        return [$fkey => $this->pid()];
    }

    /**
     * Get value of the primary key.
     * @return mixed
     */
    public function pid(){
        return $this->{$this->table->primary_key} ?? null;
    }

    /**
     * Get a ->w() argument with this models primary key and
     * its respective id.
     * @param bool $qualified whether to generate the name fully
     * qualified, i.e. table_name.foreign_key
     * @return array
     */
    public function pkey_id($qualified = false){
        $pkey = $this->table->primary_key;
        if($qualified)
            $pkey = "{$this->table->name}.$pkey";
        return [$pkey => $this->pid()];
    }

    /**
     * @param string|integer $value value set primary key to
     * Get value of the primary key.
     */
    public function set_pkey($value){
        $this->{$this->table->primary_key} = $value;
    }

    /**
     * Get a list of entities of another class from
     * its respective DB table by the reference of
     * this objects ID as foreign in the table.
     * @param $table Table the other table.
     * @param bool $single_row whether to fetch a single record.
     * @return mixed Returns an array of an AppModel
     * subclass.
     * @throws Exception
     */
    public function this_id_from($table, $single_row = false){
        $select = $single_row ? 'so' : 's';
        $result = $table->$select()->w($this->fkey_id())->r();
        return $table->update_cache($result);
    }

    /**
     * Get a list of objects that refer to this object
     * from their respective table with this objects
     * foreign ID joined over another table.
     * @param $table string Table from which we are getting the referring objects.
     * @param $join_table string Table we are joining over.
     * @return mixed Returns an array of an AppModel
     * subclass.
     * @throws Exception
     */
    public function join_this_id($table, $join_table){
        /** @var Table $table */
        $table  = $this->db->$table;
        $result = $table->s()->w($this->fkey_id(true))->jtid($join_table)->r();
        return $table->update_cache($result);
    }

    /**
     * Update several properties the model and write the new values to DB.
     * @param array $keys_values an assoc array of key=>values to be updated
     * where 'key' is the model property name and 'value' is its new value.
     * @param $skip_assignment bool whether to assign array values to the
     * model before updating. Generally is needed for bounce method only since.
     * it writes the properties that have already been set.
     * @return bool
     * @throws Exception
     */
    public function updates($keys_values, $skip_assignment = false){
        $this->check_pid();
        if(!$skip_assignment)
            foreach($keys_values as $key => $value)
                $this->$key = $value;
        return $this->table->u($keys_values)->wr($this->pkey_id());
    }

    /**
     * Update a single field of the model and write the new value to DB.
     * @param string $field field to run update on.
     * @param mixed $value new value for the field.
     * @return bool
     * @throws Exception
     */
    public function update($field, $value){
        return $this->updates([$field => $value]);
    }

    /**
     * Check if primary ID value has been set.
     * @param bool $needs_to_be_specified when set to true throw
     * exception when value is empty, when set to false throw exception
     * when value is not empty.
     * @throws LogicException when the main ID contains a blank string
     * inherited from POST transfer.
     * @throws Exception
     */
    private function check_pid($needs_to_be_specified = true){
        $val = $this->pid();
        if($val === '')
            throw new LogicException("Blank string has been supplied in the primary ID field. Most likely it was due to being submitted via a POST request instead of a null value. Set the ID value of the object to null to address it.");

        $empty = empty($val);
        if($needs_to_be_specified && $empty)
            throw new Exception("Primary ID value has not been specified");
        if(!$needs_to_be_specified && !$empty)
            throw new Exception("Primary ID has been specified already");
    }

    /**
     * Add this object to the DB context. The model will retrieve
     * the ID that was created by the DB upon record insertion and
     * update the model instance with it.
     * @param bool $keep_id whether main ID to be inserted into table.
     * @return mixed
     * @throws Exception
     */
    public function add($keep_id = false){
        $this->check_pid($keep_id);
        $fields = $this->get_assoc_fields($keep_id);

        //it will recognise that the query is insert
        //and will return the id of the inserted model
        //seems like racing condition resilient enough
        $result = $this->table->i($fields)->r();
        if(!$result)
            return null;
        $this->set_pkey($result);
        return $this->table->update_cache($this);
    }

    /**
     * Delete model.
     * @throws Exception when primary id is not specified.
     */
    public function delete(){
        $this->check_pid();
        $this->table->did($this->pid());
    }

    /**
     * Write this model to the DB overwriting all fields in the row.
     * @param string|string[] $fields fields to bounce
     * @return bool
     * @throws Exception
     */
    public function bounce($fields = []){
        $this->check_pid();
        if(is_string($fields))
            $fields = [$fields];
        if($fields == null)
            $fields = $this->get_assoc_fields();
        else {
            $collect = [];
            foreach($fields as $field)
                $collect[$field] = $this->$field;
            $fields = $collect;
        }
        /// todo: need to check the autotimestamp fields
        unset($fields[$this->table->primary_key]);
        return $this->updates($fields, true);
    }

    /**
     * Get the public fields in an assoc array. Will optionally
     * exclude capitalised and underscored field names.
     * @param bool $keep_id whether to keep primary ID.
     * @return array of public fields except those that refer
     * to objects.
     */
    public function get_assoc_fields($keep_id = true){
        //todo: might optimize that using reflection
        $fields = $this->db->object_fields($this);
        if(!$keep_id)
            unset($fields[$this->table->primary_key]);

        $checking = $fields;
        foreach($checking as $field => $value){
            if($this->table->is_field_filtered($field))
                unset($fields[$field]);
        }
        return $fields;
    }

    /**
     * Display the breakdown of public field contents.
     */
    public function dump(){
        print_r($this->get_assoc_fields());
    }
}