<?php

namespace glowbug\orm;

use Exception;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;

/**
 * Creates a mapping between a model class and a specific table in DB.
 * Instances of this class are expected to be assigned to properties of
 * DBContext subclass instance and are used to instantiate model objects
 * from DB row data. Can also retrieve specific fields as assoc arrays,
 * rather than entire rows as objects, as well as update and delete records.
 * Essentially Table class is simply an SQL statement generator that is
 * aimed to work with a specific class and a DB table.
 */
class Table {
    /**
     * DB context we are running SQL statements through.
     * DBContext is an abstract class containing conceptual
     * functionality, while DB is name could be used for
     * DBContext subclasses.
     * @var DBContext
     */
    public $db;

    /**
     * Table name in the DB.
     * @var string
     */
    public $name;

    /**
     * ID field name in the table. Defaults to 'id'.
     * @var string
     */
    public $primary_key;

    /**
     * This table's key referred by other tables as a
     * foreign key.
     * @var string
     */
    public $foreign_key;

    /**
     * SQL parameters that statement will run with.
     * @var array
     */
    public $params = [];

    /**
     * @var string[] name of the fields being fetched in select mode.
     */
    protected $fetch_fields = [];

    /**
     * Get number of model fields being fetched. Will return 0 for null
     * value, which means return an object instance.
     * @return int
     */
    protected function fetch_fields_num(){
        return count($this->fetch_fields);
    }

    /**
     * Just a single row returned.
     * @var bool
     */
    protected $one_row;

    const SELECT = 's';
    const UPDATE = 'u';
    const DELETE = 'd';
    const INSERT = 'i';

    /**
     * SQL mode, select 's', update 'u', delete 'd', insert 'i'.
     * @var string
     */
    protected $mode;

    /**
     * Type of mode action, 'sum'
     * @var mixed
     */
    protected $action;

    /**
     * Parts of the sql statement that will be glued together.
     * @var string[]
     */
    protected $sections;

    /**
     * Append a string to the query.
     * @param string $q
     * @return Table
     */
    protected function append($q){
        $this->sections[] = $q;
        return $this;
    }

    /**
     * Reset the query constructor.
     */
    protected function reset(){
        $this->params   = [];
        $this->mode     = false;
        $this->action   = '';
        $this->sections = [];
        $this->one_row  = false;
    }

    /**
     * Fully qualified primary key name.
     * @return string
     */
    public function qkey(){
        return "$this->name.$this->primary_key";
    }

    /**
     * Class name of the model this table serves.
     * @var string
     */
    public $class;

    const TS_CREATED = 'created_at';
    const TS_UPDATED = 'updated_at';
    const TS_DELETED = 'deleted_at';

    /**
     * This field contains the name of the property
     * that should be updated with current timestamp
     * when the model is created.. Won't update if not
     * specified.
     * @var string
     */
    public $create_timestamp_field = '';

    /**
     * This field contains the name of the property
     * that should be updated with current timestamp
     * when the model is updated. Won't update if not
     * specified.
     * @var string
     */
    public $update_timestamp_field = '';

    /**
     * This field contains the name of the property
     * that should be updated with current timestamp
     * when the model is deleted. Won't update if not
     * specified.
     * @var string
     */
    public $delete_timestamp_field = '';

    /**
     * Whether to skip $update_timestamp_field upon
     * model update and $create_timestamp_field when
     * model is created. Useful when the underlying
     * DB driver can take care of that automatically.
     * If set to false will automatically update the
     * autotimestamp fields upon model add and update,
     * otherwise will unset them to ensure they do not
     * overwrite the values in the DB accidentally.
     * @var bool
     */
    public $skip_auto_timestamps = false;

    /**
     * @var bool whether to unset capitalised properties on the
     * model during DB updates. Can be convenient when capitalised
     * properties are virtual, meaning hold references to objects
     * or other entities not present in the table layout.
     */
    public $skip_capitalised_props = true;

    /**
     * If caching is turned on.
     * @var bool
     */
    static public $cache_enabled = false;

    /**
     * Local Table cache array
     * @var array
     */
    private $cache = [];

    /**
     * Update cache with an object fetch result.
     * @param $result mixed
     * @return mixed
     */
    public function update_cache($result){
        if(!self::$cache_enabled)
            return $result;
        if($result){
            $_result = is_array($result)? $result : [$result];
            if(!is_object($_result[0]))
                return $result;

            /** @var AppModel $item */
            foreach($_result as $item)
                $this->cache[$item->pid()] = $item;
        }
        return $result;
    }

    /**
     * @var array model field names that contain json data.
     */
    protected $json_fields;

    /**
     * Create a model table. Result caching is enabled by default, which adds a bit of
     * cache management overhead, disable it by setting self::$cache_enabled = false.
     * Assumes its foreign key is lowercase $class . "_" . $id .
     * @param DBContext $db DB context table is being attached to.
     * @param string $class name of the model class it will map to, may include
     * namespace prefix.
     * @param string $table_name name of the table, to map to, if different from class.
     * If class name contains namespace prefix, will trim the base class name out.
     * @param string $id name of the primary ID key in the table
     * (not case sensitive for MySQL)
     */
    public function __construct($db, $class, $table_name = '', $id = 'id'){
        if(!$table_name){
            $nsPos      = strrpos($class, '\\');
            $table_name = $nsPos !== false? substr($class, $nsPos + 1) : $class;
        }

        $this->db    = $db;
        $this->class = $class;
        $this->name  = $table_name;

        $this->primary_key = $id;
        $this->foreign_key = strtolower($table_name) . "_$id";

        /// Attempting to detect auto timestamp fields
        foreach(DBContext::class_fields($class) as $field)
            switch($field){
                case self::TS_CREATED:
                    $this->create_timestamp_field = self::TS_CREATED;
                    break;
                case self::TS_UPDATED:
                    $this->update_timestamp_field = self::TS_UPDATED;
                    break;
                case self::TS_DELETED:
                    $this->delete_timestamp_field = self::TS_DELETED;
                    break;
            }

        /** @var AppModel $class */
        $this->json_fields = $class::json_fields();
    }

    /**
     * Generate select statement.
     * @param array|null $fields numeric array of fields to be fetched. Return * if null.
     * @param bool $one_row if only one row should be returned.
     * @return Table
     * @throws Exception it was possible to submit a string for $fields before.
     */
    public function s($fields = [], $one_row = false){
        /// todo: when no where clause supplied it fetches soft deleted records as well
        if(is_string($fields))
            throw new Exception('A string is supplied to the SELECT statement, an array needed.');
        $this->reset();

        $this->mode         = self::SELECT;
        $this->one_row      = $one_row;
        $this->fetch_fields = $fields;

        $select = $this->fetch_fields_num()? implode(", ", $fields) : "$this->name.*";
        $this->append("select $select from $this->name");
        return $this;
    }

    /**
     * Get all models from a table.
     * @return mixed
     * @throws Exception
     */
    public function all(){
        return $this->swr();
    }

    /**
     * Retrieve the first row(s) from the query result.
     * @param int $num The number of rows to return. If negative,
     * last rows will be returned instead.
     * @return array|object
     */
    public function first($num = 1){
        if($num < 0)
            $result = $this->s()->od()->l(abs($num))->r();
        else $result = $this->s()->l($num)->r();
        return count($result) == 1? $result[0] : $result;
    }

    /**
     * Retrieve the last few records from a list.
     * @param int $num The number of records to retrieve.
     * @return array|object
     */
    public function last($num = 1){
        return $this->first(-$num);
    }

    /**
     * Return just a single row.
     * @param array $fields numeric array of fields to be fetched. Return * if null.
     * @return Table
     */
    public function so($fields = []){
        return $this->s($fields, true);
    }

    /**
     * Select a single id for a given where clause.
     * @param $where array where params of the query.
     * @return int
     */
    public function soid($where = []){
        return $this->so([$this->primary_key])->wr($where);
    }

    /**
     * Check if a record with a specific id exists.
     * @param $id int|string object id.
     * @return int id|null of the record, or null.
     */
    public function has_id($id){
        return $this->soid([$this->primary_key => $id]);
    }

    /**
     * Verifies if there is a field with a given value.
     * Convenient for checking if an object with a specific unique field exists.
     * @param string $field name of the field verified.
     * @param string|int $value that should be contained in field.
     * @return string|int the ID of the DB row containing the value in the field.
     */
    public function value_exists($field, $value){
        return $this->so([$this->primary_key])->w([$field => $value])->r();
    }

    /**
     * Return a single model with a given ID, or a list of those
     * for an array of ids. Will ensure that array is unique.
     * @param int|int[] $id a single id, or an array of those.
     * @return mixed
     * @throws Exception
     */
    public function id($id){
        if(is_array($id))
            return $this->swr(['in' => [$this->primary_key => array_unique($id)]]);
        return $this->sowr([$this->primary_key => $id]);
    }

    /**
     * Return a single field value by the primary ID.
     * @param string|int $id primary ID value
     * @param string|int $field column whose value is retrieved
     * @return string|int
     */
    public function f($id, $field){
        return $this->so([$field])->wr([$this->primary_key => $id]);
    }

    /**
     * Get the object with the highest ID in the table.
     * @return mixed
     */
    public function get_last(){
        return $this->so()->od()->r();
    }

    /**
     * Get the most recent ID of the table.
     * @return int
     */
    public function get_last_ID(){
        return $this->so([$this->primary_key])->od()->r();
    }

    /**
     * Count number of rows present matching a where condition.
     * @param string|array $where
     * @return int
     */
    public function c($where = []){
        return $this->so(["count(*)"])->wr($where);
    }

    /**
     * Total of values in a specific field (where clause can be applied).
     * @param string $field name of the field whose value is being summed
     * @param string|array $where
     * @return int
     */
    public function sum($field, $where){
        return $this->so(["sum($field)"])->wr($where);
    }

    /**
     * Generate current time stamp.
     * @return string
     */
    private function date(){
        return date('Y-m-d H:i:s');
    }

    /**
     * Json encode supplied value. The difference from the stock
     * json_encode() is that it will encode empty array as an {}.
     * @param $val mixed
     * @return string
     */
    private function json_encode($val){
        if(is_array($val) and !count($val))
            return "{}";
        return json_encode($val);
    }

    /**
     * Generate update statement.
     * @param array $fields_values assoc array with field=>value update params.
     * @return Table
     */
    public function u($fields_values){
        if($utf = $this->update_timestamp_field){
            if($this->skip_auto_timestamps)
                unset($fields_values[$utf]);
            else
                $fields_values[$utf] = $this->date();
        }

        foreach($this->json_fields as $field)
            if(isset($fields_values[$field]))
                $fields_values[$field] = $this->json_encode($fields_values[$field]);

        $out = [];
        $this->reset();
        $this->mode = self::UPDATE;
        foreach($fields_values as $field => $value)
            $out[] = "$field=:$field";
        $out = implode(", ", $out);
        $this->append("update $this->name set $out");
        $this->params = $fields_values;
        return $this;
    }

    /**
     * Set update and create timestamps.
     * @param $fields_values array
     * @param null|string $date date to set the fields, will default to
     * current timestamp if not specified
     * @return array the original array
     * @throws Exception
     */
    private function update_timestamps(&$fields_values, $date = null){
        if(!$date)
            $date = $this->date();
        if($ctf = $this->create_timestamp_field)
            $fields_values[$ctf] = $date;
        if($utf = $this->update_timestamp_field)
            $fields_values[$utf] = $date;
        return $fields_values;
    }

    /**
     * Unset update and create timestamps.
     * @param $fields_values array
     * @return array the original array
     */
    private function unset_timestamps(&$fields_values){
        if($ctf = $this->create_timestamp_field)
            unset($fields_values[$ctf]);
        if($utf = $this->update_timestamp_field)
            unset($fields_values[$utf]);
        return $fields_values;
    }

    /**
     * Generate insert statement.
     * @param array $fields_values assoc array with field=>value insert params
     * @return Table
     */
    public function i(&$fields_values){
        if(!$this->skip_auto_timestamps)
            $this->update_timestamps($fields_values);
        else $this->unset_timestamps($fields_values);

        foreach($this->json_fields as $field)
            //if(isset($fields_values[$field]))
            $fields_values[$field] = $this->json_encode($fields_values[$field]);

        $this->reset();
        $this->mode = self::INSERT;
        $fields     = array_keys($fields_values);

        $values = implode(', :', $fields);
        $fields = implode(', ', $fields);
        $this->append("insert into $this->name ($fields) values (:$values)");
        $this->params = $fields_values;
        return $this;
    }

    /**
     * Check if a field name should be filtered, that is if it starts with a
     * capital letter and self::$skip_capitalised_props is set to true, or
     * the first character in the name is an underscore.
     * @param $field_name
     * @return bool
     */
    public function is_field_filtered($field_name){
        $code    = ord($field_name[0]);
        $capital = $this->skip_capitalised_props && $code > 64 && $code < 91;
        return $capital || $code == 95;
    }

    /**
     * Generate insert statement (insert many) for multiple adds.
     * @param array[] $fields_values array of assoc arrays with field=>value insert params
     * @return Table
     */
    public function insert_many(&$fields_values){
        if(!$this->skip_auto_timestamps){
            $date = $this->date();
            foreach($fields_values as &$fields_value)
                $this->update_timestamps($fields_value, $date);
        } else
            foreach($fields_values as &$fields_value)
                $this->unset_timestamps($fields_value);

        foreach($fields_values as &$fields_value){
            foreach($this->json_fields as $field)
                //if(isset($fields_values[$field]))
                $fields_value[$field] = $this->json_encode($fields_value[$field]);
        }

        $fields = [];
        $values = [];
        $this->reset();
        $this->mode = self::INSERT;

        foreach($fields_values[0] as $field => $value)
            //if(!$this->is_field_filtered($field))
            $fields[] = $field;

        $i = 0;
        foreach($fields_values as $field_value){
            $vals = [];
            foreach($field_value as $field => $value){
                $id                = $field . "_$i";
                $vals[]            = ":$id";
                $this->params[$id] = $value;
            }
            $vals     = implode(', ', $vals);
            $values[] = "($vals)";
            $i++;
        }

        $fields = implode(', ', $fields);
        $values = implode(', ', $values);
        $this->append("insert into $this->name ($fields) values $values");
        return $this;
    }

    /**
     * Add a bunch of models to the table. Does not update their IDs.
     * @param $objects array
     * @param bool $keep_id whether to insert primary ID into table.
     * @return Model[]
     * @throws Exception
     */
    public function add($objects, $keep_id = false){
        if(!$objects)
            return [];
        $collect = [];
        /** @var Model $object */
        foreach($objects as $object)
            $collect[] = $object->get_assoc_fields($keep_id);
        $this->insert_many($collect)->r();
        return $objects;
    }

    /**
     * Increment a numeric field in the table.
     * @param $id int|string row ID in table.
     * @param $field string field name to update.
     * @param $amount int
     * @return bool
     */
    public function incf($id, $field, $amount = 1){
        return $this->u([$field => "$field + $amount"])->w([$this->primary_key => $id])->r() == 1;
    }

    /**
     * Generate hard delete statement. Do not forget to supply a where clause, otherwise the entire table will be cleared.
     * If where clause was supplied, will run the query straight-away.
     * @param array|string $where optional where clause.
     * @param bool $force hard delete records from db even if $delete_timestamp_field is present.
     * @return Table
     */
    public function d($where = [], $force = false){
        $delete = function() use ($force){
            if($this->delete_timestamp_field && !$force)
                return $this->u([$this->delete_timestamp_field => $this->date()]);
            $this->reset();
            $this->mode = self::DELETE;
            $this->append("delete from $this->name");
            return $this;
        };
        if($where)
            $delete()->wr($where);
        else $delete();
        return $this;
    }

    /**
     * @return bool will return false if delete timestamp not present,
     * true will be returned if a force delete query was run.
     */
    public function purge(){
        if(!$field = $this->delete_timestamp_field)
            return false;
        $this->db->query("delete from $this->name where $field is not null");
        return true;
    }

    /**
     * Delete a record by its id.
     * @param int $id
     * @param bool $force hard delete records from db even if $delete_timestamp_field is present.
     * @return $this
     */
    public function did(int $id, $force = false){
        return $this->d([$this->primary_key => $id], $force);
    }

    /**
     * Generate a where clause.
     * todo: need to take into account that without w() deleted records won't be skipped by selects
     * @param mixed $where either string or an assoc array or conditions. String is recommended to be used unless SQL parameters have to be used. This clause also sets up a local cache key.
     * @return Table
     */
    public function w($where = []){
        if(!$where && $this->delete_timestamp_field)
            return $this->append("where $this->delete_timestamp_field is null");

        if($this->mode == self::INSERT)
            throw new Exception("Cannot use where clauses on insert statements.");
        if(is_array($where))
            $where = $this->intersperse($where);
        if($del = $this->delete_timestamp_field)
            $where = "$del is null and ($where)";
        return $this->append("where $where");
    }

    /**
     * Generate s()->r() statement.
     * @param array $fields
     * @return mixed
     * @throws Exception
     */
    public function sr($fields = []){
        return $this->s($fields)->r();
    }

    /**
     * Generate so()->r() statement.
     * @param array $fields
     * @return mixed
     * @throws Exception
     */
    public function sor($fields = []){
        return $this->so($fields)->r();
    }

    /**
     * Generate a select where struct similar to s()->w($where).
     * @param $where string|array
     * @return Table
     * @throws Exception
     */
    public function sw($where = []){
        return $this->s()->w($where);
    }

    /**
     * Generate a select one where struct similar to so()->w($where).
     * @param $where string|array
     * @return Table
     * @throws Exception
     */
    public function sow($where = []){
        return $this->so()->w($where);
    }

    /**
     * Generate a where clause and run the query.
     * @param mixed $where either string or an assoc array or conditions. String is recommended to be used unless SQL parameters have to be used. This clause also sets up a local cache key.
     * @return mixed
     */
    public function wr($where = []){
        return $this->w($where)->r();
    }

    /**
     * Generate a select multiple objects query with a where clause and run the query.
     * $this->s()->w($where)->r();
     * @param mixed $where either string or an assoc array or conditions. String is recommended to be used unless SQL parameters have to be used. This clause also sets up a local cache key.
     * @return mixed
     */
    public function swr($where = []){
        return $this->s()->w($where)->r();
    }

    /**
     * Generate a select a single object statement with a where clause and run the query.
     * $this->so()->w($where)->r();
     * @param mixed $where either string or an assoc array or conditions. String is recommended to be used unless SQL parameters have to be used. This clause also sets up a local cache key.
     * @return mixed
     */
    public function sowr($where = []){
        return $this->so()->w($where)->r();
    }

    /**
     * Generate a where clause for a specific ID and run the query.
     * @param mixed $id the main ID of the row.
     * @return mixed
     */
    public function wid($id){
        return $this->wr([$this->primary_key => $id]);
    }

    /**
     * Generate a where clause for a list of ids and run the query.
     * @param int[]|string[] $ids the main ID of the row.
     * @return mixed
     */
    public function wids($ids){
        return $this->wr(['in' => [$this->primary_key => $ids]]);
    }

    /**
     * Get a model by a field value.
     * @param string $val field value to search by.
     * @param string $field field to check, default is url_slug.
     * @return mixed
     */
    public function by_val(string $val, string $field = 'url_slug'){
        return $this->sowr([$field => $val]);
    }

    /**
     * Get many model by a field value.
     * @param string $val field value to search by.
     * @param string $field field to check, default is url_slug.
     * @return mixed
     */
    public function by_valm(string $val, string $field){
        return $this->swr([$field => $val]);
    }

    /**
     * Fetch records from the table that have a field value
     * in the supplied list.
     * @param array $array containing the IN clause values.
     * @param string $field DB table to check, will default
     * to the primary id of the table.
     * @return mixed
     */
    public function in($array, $field = ''){
        if(!$field)
            $field = $this->primary_key;
        return $this->swr(['in' => [$field => $array]]);
    }

    /**
     * @param $where array assoc array where each key
     * corresponds to a table field name, and its value
     * is an array of values to supply to the IN clause.
     * @return mixed
     */
    public function ins($where){
        return $this->swr(['in' => $where]);
    }

    /**
     * Generate order by clause.
     * @param string $order_by the name of the field to order by. Will sort by primary ID if null.
     * @return Table
     */
    public function o($order_by = null){
        if(!$order_by)
            $order_by = $this->primary_key;
        return $this->append("order by $order_by asc");
    }

    /**
     * Generate descending order by clause.
     * @param string $order_by_desc the name of the field to order by descending. Will sort by primary ID if null.
     * @return Table
     */
    public function od($order_by_desc = null){
        if(!$order_by_desc)
            $order_by_desc = $this->primary_key;
        return $this->append("order by $order_by_desc desc");
    }

    /**
     * Generate a limit clause.
     * @param int $limit number of rows to return.
     * @return Table
     */
    public function l($limit = 50){
        return $this->append("limit $limit");
    }

    /**
     * Generate join clause. todo: need to get the join clauses in shape, finally
     * @param string $this_field the joined field in this table.
     * @param Table|string $other_table the other table object being joined to, or its string name.
     * @param string $other_field the field in the other table joining on.
     * @return Table
     */
    public function j($this_field, $other_table, $other_field){
        if(!is_string($other_table))
            $other_table = $other_table->name;
        return $this->append("inner join $other_table on $this->name.$this_field=$other_table.$other_field");
    }

    /**
     * Join a foreign key in the current table to the primary ID key in another.
     * @param Table|string $other_table the other table object, or its DB name.
     * @return Table
     */
    public function joid($other_table){
        return $this->j($this->foreign_key, $other_table, $other_table->primary_key);
    }

    /**
     * Join ID of the current table onto the foreign key in another table that refers to the ID field of the current one.
     * @param Table|string $other_table the other table object, or its DB name.
     * @return Table
     */
    public function jtid($other_table){
        return $this->j($this->primary_key, $other_table, $this->foreign_key);
    }

    /**
     * Generate group clause.
     * @param string $field name of the field to group by.
     * @return Table
     */
    public function g($field){
        return $this->append("group by $field");
    }

    /**
     * Format the currently constructed query.
     * @return string
     */
    public function q(){
        return implode(' ', $this->sections);
    }

    /**
     * Run the SQL query accumulated on the DB context.
     * @return mixed
     */
    public function r(){
        $run = function(){
            if(!$this->mode)
                throw new Exception('SQL statement mode has not been specified.');
            try {
                return match ($this->mode) {
                    self::SELECT => $this->fetch(),
                    self::INSERT => $this->db->insert($this->q(), $this->params),
                    default      => $this->db->execute($this->q(), $this->params),
                };
            } catch(Exception $ex) {
                $message = $ex->getMessage() . " Statement: " . $this->q();
                throw new Exception(substr($message, 0, 1000), intval($ex->getCode()));
            }
        };
        return $this->update_cache($run());
    }

    /**
     * Execute select mode statement.
     * @return mixed
     * @throws Exception
     */
    protected function fetch(){
        $query = $this->q();
        switch($this->fetch_fields_num()){
            case 1:
                $is_json = in_array($this->fetch_fields[0], $this->json_fields);
                if($this->one_row){
                    $result = $this->db->field_row($query, $this->params);
                    if($is_json && $result)
                        $result = json_decode($result, true);
                    return $result;
                } else {
                    $results = $this->db->field_rows($query, $this->params);
                    if($is_json && $results){
                        $out = [];
                        foreach($results as $result)
                            $out[] = json_decode($result, true);
                        $results = $out;
                    }
                    return $results;
                }
            case 0:
                $json_model = function($model){
                    foreach($this->json_fields as $json_field)
                        $model->$json_field = json_decode($model->$json_field, true);
                };

                if($this->one_row){
                    $result = $this->db->object($query, $this->class, $this->params, [$this]);
                    if($result)
                        $json_model($result);
                    return $result;
                } else {
                    $results = $this->db->objects($query, $this->class, $this->params, [$this]);
                    if($results)
                        foreach($results as $result)
                            $json_model($result);
                    return $results;
                }
            default:
                $json_array = function(&$array){
                    foreach($this->json_fields as $json_field)
                        if(isset($array[$json_field]))
                            $array[$json_field] = json_decode($array[$json_field], true);
                };
                if($this->one_row){
                    $result = $this->db->fields_row($query, $this->params);
                    if($result)
                        $json_array($result);
                    return $result;
                } else {
                    $results = $this->db->fields_rows($query, $this->params);
                    if($results)
                        foreach($results as &$result)
                            $json_array($result);
                    return $results;
                }
        }
    }

    /**
     * @var string[] list of comparisons used in where clauses.
     */
    private $comparisons = [
        '=',
        '!=',
        '<',
        '>',
        '<=',
        '>=',
        'is',
        'is not',
        'in',
        'not in',
        'like',
        'not like'
    ];

    /**
     * A rather contrived recursive constructor of where clauses out of arrays. Array can contain assoc subarrays that may have different comparisons, or joiners.
     * E.g. ['or' => ['ID' => 100, 'Name' => 'Dandara']] will expand to
     * "where ID=100 or Name='Dandara'"
     * In a similar fashion ['!=' => ['ID' => 100, 'Name' => 'Dandara']] will expand to
     * "where ID!=100 and Name!='Dandara'", or
     * ['or' => ['!=' => ['ID' => 100, 'Name' => 'Dandara']]] will end up as
     * "where ID!=100 or Name!='Dandara'"
     *
     * @param array $where assoc of keys/values to be joined.
     * @param string $comparison key to value comparison, i.e. ID=x, or Name!='y'.
     * @param string $clause joining of subclauses by 'and' or 'or'.
     * @return string
     */
    protected function intersperse($where, $comparison = '=', $clause = 'and'){
        $fields = [];
        static $count = 0;

        foreach($where as $tag => $subWhere)
            if($tag == 'or' or $tag == 'and'){
                $intersperse = $this->intersperse($subWhere, $comparison, $tag);
                $fields[]    = "($intersperse)";
            } else if(in_array($tag, $this->comparisons)){
                $intersperse = $this->intersperse($subWhere, $tag, $clause);
                $fields[]    = $intersperse;
            } else {
                ///here we are joining actual fields and their values
                if($comparison == 'in' || $comparison == 'not in'){
                    if(is_array($subWhere))
                        $subWhere = "('" . implode("', '", $subWhere) . "')";
                    $fields[] = "$tag $comparison $subWhere";
                    continue;
                }
                $renamed  = str_replace('.', '_', $tag) . "_w" . $count++;
                $fields[] = "$tag $comparison :$renamed";

                $this->params[$renamed] = $subWhere;
            }

        return implode(" $clause ", $fields);
    }

    /**
     * Create a DB drop table query.
     * @param false $run whether to run the resulting query.
     * @return string
     */
    public function drop_db_table($run = false){
        $query = "DROP TABLE IF EXISTS `$this->name`;";
        if($run)
            $this->db->exec($query);
        return $query;
    }

    /**
     * Create a DB create query for a specific DB driver for this table.
     * @param bool $run whether to run the resulting query.
     * @param string $default default table field type.
     * @return string
     * @throws ReflectionException
     */
    public function create_db_table($run = false, $default = 'TEXT NULL'){
        $refClass = new ReflectionClass($this->class);
        $props    = $refClass->getProperties(ReflectionProperty::IS_PUBLIC);

        $fields     = [];
        $regex      = "/(?<=@{$this->db->driver}).+\\v/";
        $isSqlsrv   = $this->db->driver == DBContext::SQLSRV;
        $sqlSrvFn   = function($name, $val){
            return "\t[$name] $val";
        };
        $noSqlSrvFn = function($name, $val){
            return "\t`$name` $val";
        };
        $makeField  = $isSqlsrv? $sqlSrvFn : $noSqlSrvFn;
        foreach($props as $prop){
            $name = $prop->getName();
            if(ctype_upper($name[0]))
                continue; //skip capitalized props as those are virtual
            $input = $prop->getDocComment();
            preg_match($regex, $input, $out);
            if(!$out){
                if($default)
                    $fields[] = $makeField($name, $default);
                continue;
            }
            $fields[] = $makeField($name, strtoupper(trim($out[0])));
        }
        $fields = implode(",\n", $fields);

        $query = match ($this->db->driver) {
            DBContext::SQLITE => "CREATE TABLE `$this->name` (\n$fields\n);",
            DBContext::MYSQL  => "CREATE TABLE `$this->name` (\n$fields,\nPRIMARY KEY(`$this->primary_key`));",
            DBContext::SQLSRV => "CREATE TABLE [dbo].[$this->name] (\n$fields,\nCONSTRAINT [{$this->name}_PK] PRIMARY KEY CLUSTERED ([$this->primary_key] ASC));",
            default           => throw new Exception("Table creation not implemented for {$this->db->driver} DB driver."),
        };
        if($run)
            $this->db->exec($query);
        return $query;
    }
}
