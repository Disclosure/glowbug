<?php

namespace glowbug\orm;

/*
 * A helper class that extends DBContext with some functionality specific
 * to development of end user web applications.
 */
class AppDB extends DBContext {
    public function __construct($driver, $host, $dbname, $username, $password, $create_if_not_exists = false){
        parent::__construct($driver, $host, $dbname, $username, $password, $create_if_not_exists);
    }

    /**
     * Get user by their email and password credentials.
     * @param $model string DB table model name.
     * @param $email string
     * @param $pass string
     * @return AppUser
     */
    public function get_user_by_email($model, $email, $pass){
        return $this->$model->so()->wr([
            'email'    => $email,
            'password' => $pass
        ]);
    }

    /**
     * Create a unique slug for model table.
     * @param $model string name of the model class.
     * @param string $col_name name of the slug field.
     * @return string
     */
    public function make_slug($model, $col_name = "url_slug"){
        do {
            $slug = random_string(12);
        } while ($this->$model->value_exists($col_name, $slug));
        return $slug;
    }
}