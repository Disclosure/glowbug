<?php

namespace glowbug\orm;

/**
 * Class AppModel is a base class for all classes
 * based on DB tables.
 * todo: merge with Model at some stage
 */
abstract class AppModel extends Model {

    /**
     * @var int|string main ID
     * @mysql int(10) unsigned not null auto_increment
     * @sqlite integer primary key
     * @sqlsrv int identity (1, 1) not null
     */
    public $id;
    const id = 'id';

    /**
     * @var null|string datetime when DB record created.
     * @mysql datetime not null
     * @sqlsrv datetime not null
     */
    public $created_at = null;
    const created_at = Table::TS_CREATED;

    /**
     * @var null|string datetime when DB record updated.
     * @mysql datetime not null
     * @sqlsrv datetime not null
     */
    public $updated_at = null;
    const updated_at = Table::TS_UPDATED;

    /**
     * @var null|string datetime when DB record deleted.
     * @mysql datetime null
     * @sqlsrv datetime null
     */
    public $deleted_at = null;
    const deleted_at = 'deleted_at';

    /**
     * @var array fields that allowed to be updated via post.
     */
    protected static $post_fields = [];

    /**
     * @return string[] list of fields that are allowed
     * to be updated from POST.
     */
    public static function post_fields(){
        return static::$post_fields;
    }

    /**
     * Is the field one of post fields for the model.
     * @param $field string field name.
     * @return bool
     */
    public static function is_post_field($field){
        return in_array($field, static::$post_fields);
    }

    /**
     * Update post fields in the DB table from this instance.
     */
    public function bounce_post_fields(){
        $this->bounce(static::post_fields());
    }

    /**
     * todo: public fields should be assoc array with compulsory fields set to true
     * @var array fields that allowed to be served to frontend.
     */
    protected static $public_fields = [];

    /**
     * @return string[] list of fields that are allowed
     * to be served to frontend.
     */
    public static function public_fields(){
        return static::$public_fields;
    }

    /**
     * Is the field one of public fields for the model.
     * @param $field string field name.
     * @return bool
     */
    public static function is_public_field($field){
        return in_array($field, static::$public_fields);
    }

    /**
     * Return an array of public fields, handy when it is needed
     * to turn an internal model into a public set of properties.
     * @return array of fields
     */
    public function conceal_fields(){
        $result = [];
        foreach(static::public_fields() as $field)
            $result[$field] = $this->$field;
        return $result;
    }

    /**
     * Create class instance from assoc array.
     * @param $table Table DB table for class.
     * @param $array string[] key value pairs.
     * todo: auto initialise virtual properties
     * @return mixed
     */
    public static function from_array($table, $array){
        if(!$array)
            return null;
        $model = new $table->class($table);
        foreach(static::$post_fields as $field)
            $model->$field = $array[$field];
        return $model;
    }

    /**
     * Create a static model from post vars.
     * @param $table Table DB table for class.
     * @return mixed
     */
    public static function from_post($table){
        $model = static::class;
        $new   = new $model($table);
        foreach($new::post_fields() as $update_field)
            $new->$update_field = htmlentities($_POST[$update_field]);
        return $new;
    }
}
