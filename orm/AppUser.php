<?php

namespace glowbug\orm;

/**
 * Class User contains data pertinent to various user entities.
 */
abstract class AppUser extends AppModel {

    /**
     * @var string
     * @mysql VARCHAR(50) NOT NULL
     */
    public $email;

    /**
     * @var string
     * @mysql CHAR(40) NOT NULL
     */
    public $password;

    /**
     * @var null|string
     * @mysql CHAR(20) NULL
     */
    public $pass_reset = null;

    /**
     * @var string
     * @mysql VARCHAR(30) NOT NULL
     */
    public $first_name;

    /**
     * @var string
     * @mysql VARCHAR(30) NOT NULL
     */
    public $last_name;

    /**
     * @var string
     * @mysql VARCHAR(32) NOT NULL
     */
    public $phone = '';

    /**
     * @var string
     * @mysql char(2) not null
     */
    public $country;

    /**
     * @var string
     * @mysql VARCHAR(64) NOT NULL
     */
    public $city;

    /**
     * @var string
     * @mysql VARCHAR(128) NOT NULL
     */
    public $address_1;

    /**
     * @var string
     * @mysql VARCHAR(128) NOT NULL
     */
    public $address_2 = '';

    /**
     * @var string
     * @mysql VARCHAR(20) NOT NULL
     */
    public $postcode = '';

    const IE = 'IE';
    const GB = 'GB';
    const AU = 'AU';
    const US = 'US';

    //public $iban = null; //may need it for both clients and debtors

    /**
     * Get the country name from the country code.
     * @return string
     */
    public function country_name(){
        return match ($this->country) {
            AppUser::IE => 'Ireland',
            AppUser::GB => 'United Kingdom',
            AppUser::AU => 'Australia',
            AppUser::US => 'United States',
            default     => $this->country,
        };
    }

    static public function make_password_hash($string = '', $salt = "here is a nice salt string"){
        if (!$string)
            $string = random_string(16);
        return sha1($string . $salt);
    }

    /**
     * Get full name of the user.
     * @param string $separator
     * @param bool $lowercase
     * @return string
     */
    public function full_name($separator = ' ', $lowercase = false){
        $full = $this->first_name . $separator . $this->last_name;
        return $lowercase ? strtolower($full) : $full;
    }

    /**
     * Create a new user instance.
     * @param $table Table
     * @param string $pass
     * @return AppUser
     */
    public static function from_post($table, $pass = 'test'){
        $user = self::from_post($table);
        ///db should be an instance of AppModel to have make_slug()
        $user->url_slug = $table->db->make_slug(static::class);
        $user->password = AppUser::make_password_hash($pass);
        return $user;
    }

    public static function make_phone_link($ph){
        $tel = str_replace(" ", "", $ph);
        return "<a href='tel:$tel'>$ph</a>";
    }

    /**
     * Generate an html telephone link.
     * @return string
     */
    public function phone_link(){
        return self::make_phone_link($this->phone);
    }

    /**
     * Generate an html email link.
     * @return string
     */
    public function email_link(){
        return "<a href='mailto:$this->email'>$this->email</a>";
    }
}
