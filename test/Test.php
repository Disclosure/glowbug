<?php

namespace glowbug\test;

use Exception;

/**
 * Class Test is used a boilerplate for custom test class creation.
 */
abstract class Test {
    /**
     * @var string current method being tested.
     */
    public $test_method;

    /**
     * @var string the name of the subclass being tested.
     */
    public $test_class;

    /**
     * @var string[] list of test methods that should not be tested.
     */
    public $skip_methods = [];

    /**
     * @var string[] list of allowed test methods, can be used when
     * only a small selection of tests needs to be run.
     */
    public $allow_methods = [];

    /**
     * @var string
     */
    protected $passed, $failed;

    /**
     * CLI colorize string (used for echoing).
     * @param $string
     * @param $code
     * @return string
     */
    protected function colour($string, $code){
        //http://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html
        return "\e[{$code}m$string\e[0m";
    }

    public function __construct(){
        $this->passed = $this->colour('passed', 32);
        $this->failed = $this->colour('failed', 31);
    }

    /**
     * Set a POST var to a value.
     * @param $var string POST variable to set.
     * @param $val mixed value to set the variable to.
     * @return mixed
     */
    protected function set_post($var, $val){
        $_POST[$var] = $val;
        return $val;
    }

    /**
     * Populate POST vars from a mock-up model.
     * @param $model mixed model object whose fields to use to populate post.
     * @param $fields string[] list of field names to populate POST vars from model.
     * @return mixed
     */
    protected function populate_post_from_model($model, $fields){
        foreach($fields as $f)
            $this->set_post($f, $model->$f);
        return $model;
    }

    /**
     * Assert equality of fields of two models.
     * @param $expected mixed object model to verify fields against.
     * @param $actual mixed object model being verified.
     * @param $fields string[] field names to compare.
     */
    protected function verify_model_fields($expected, $actual, $fields){
        $name = get_class($expected);
        foreach($fields as $f)
            $this->assert($expected->$f == $actual->$f, "$name field '$f'");
    }

    /**
     * Get response from exception message encoded in json. Useful for
     * testing http endpoints that respond via exceptions when testing in cli.
     * @param $ex Exception
     * @param string $field field to extract from message, expects an error message by default.
     * @return string
     */
    protected function get_message($ex, $field = 'error'){
        return json_decode($ex->getMessage())->$field;
    }

    /**
     * Display whether test result evaluated to true.
     * @param $condition bool
     * @param string $note to append to the test result.
     * @return string
     */
    public function assert($condition, $note = '', $err_note = ''){
        $result = $condition? $this->passed : $this->failed;
        $err    = ($condition or !$err_note)? "" : ", $err_note";
        if(!$condition){// append line num
            $backtrace = debug_backtrace();
            $file      = basename($backtrace[0]['file']);
            $line      = basename($backtrace[0]['line']);
            $err       .= " in $file:$line";
        }
        echo "$this->test_class::$this->test_method\t$result for $note$err.\n";
        return $condition;
    }

    /**
     * Verify if two arguments are equal.
     * @param $expected
     * @param $result
     * @param string $note
     * @return bool
     */
    public function equal($expected, $result, $note = ''){
        $condition = $expected == $result;
        $outcome   = $condition? $this->passed : "$this->failed expected '$expected', was '$result'";
        echo "$this->test_class::$this->test_method\t$outcome for $note\n";
        return $condition;
    }

    /**
     * Retrieve a random element from the provided array.
     * @param array $array from which to select a random element.
     * @return mixed randomly selected element.
     */
    public function get_random_element($array){
        return $array[array_rand($array)];
    }
}
