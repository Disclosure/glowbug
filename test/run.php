<?php

/// Loading global project conf that should take care of
/// class autoloading. It is assumed that all test classes
/// are located in ./tests/ of the main class directory.
require_once __DIR__ . "/../autoload.php";

/// List of class names to test.
$testClasses = [
    //'tests\\ServiceTest',
    //'tests\\MetaTest',
    'tests\\IccamTest'
];
if(isset($argv[1])) //can override from command line
    $testClasses = ["tests\\$argv[1]"];

foreach($testClasses as $tc){
    /** @var glowbug\test\Test $class */
    $class             = new $tc;
    $class->test_class = $tc;
    $skip              = $class->skip_methods;
    $allow             = $class->allow_methods;
    $methods           = get_class_methods($tc);
    foreach($methods as $m){
        if($allow and !in_array($m, $allow))
            continue;
        if(!str_starts_with($m, 'test_') or in_array($m, $skip))
            continue;
        $class->test_method = $m;
        $class->$m();
        echo "\n";
    }
}
