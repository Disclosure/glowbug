<?php

namespace glowbug\misc;

use Exception;

/**
 * Used to make remote http requests, generally for sending data to remote endpoints.
 */
class Sender {
    /**
     * @var bool whether to ignore remote endpoint non 200 http statuses.
     * If set to false will throw an exception when a non 200 code encountered.
     */
    public $ignore_post_errors;

    /**
     * @var ?int http code to return when domain is not reachable. If set to null will
     * throw an exception with a 404 code.
     */
    public $unreachable_domain_status = null;

    /**
     * @var array response headers after remote endpoint call.
     */
    public $response_headers;

    /**
     * @var string http response status after communication with remote endpoint.
     */
    public $http_status;

    /**
     * @var int http status code after communication with the remote endpoint.
     */
    public $http_code;

    /**
     * @var string this field can be used to supply cookies to the request.
     * Has to be in flattened format like user=3345; pass=abcd
     */
    public $cookies = '';

    /**
     * @var bool false whether to check the ssl cert issuer etc. It may be a good
     * idea to keep enabled however it appears to fail for letsencrypt certs.
     */
    private $verify_ssl;

    /**
     * @var bool whether to decode all responses from json into arrays.
     */
    public $json_decode = true;

    /**
     * @var bool whether to base64 encode Authorization headers.
     */
    public $encode_auth = true;

    /**
     * Check whether the last request was successful (http status is 200).
     * Does not check if request has been made, just checks the http code.
     * @return bool
     */
    public function success(){
        return $this->http_code == 200;
    }

    public function __construct($ignore_post_errors = true, $verify_ssl = false, $unreachable_domain_status = null){
        $this->verify_ssl                = $verify_ssl;
        $this->ignore_post_errors        = $ignore_post_errors;
        $this->unreachable_domain_status = $unreachable_domain_status;
    }

    /// auth types consts
    const Basic  = 'Basic';
    const Bearer = 'Bearer';

    /// common headers
    const Cookie        = 'Cookie';
    const Connection    = 'Connection';
    const Content_Type  = 'Content-Type';
    const Authorization = 'Authorization';

    /// methods consts
    const GET    = 'GET';
    const PUT    = 'PUT';
    const POST   = 'POST';
    const DELETE = 'DELETE';

    /// content types consts
    const CT_TEXT = 'text/plain';
    const CT_JSON = 'application/json';
    const CT_FORM = 'application/x-www-form-urlencoded';

    /**
     * Format authentication header value.
     * @param $auth string|array authorization token, if a string supplied then
     * Basic auth is assumed, otherwise a one element assoc array can be supplied
     * like ['Bearer' => 'some auth key here']. Auth key will be base64 encoded if
     * $this->encode_auth is set to true. If an Authorization header is supplied,
     * it will override the auth parameter if it is present in the method call.
     * @return string
     */
    public function format_auth($auth){
        if(is_string($auth))
            $type = self::Basic;
        else {
            $type = array_key_first($auth);
            $auth = $auth[$type];
        }
        return "$type " . ($this->encode_auth? base64_encode($auth) : $auth);
    }

    /**
     * Format headers from key=>value format.
     * @param array $headers key=>value list of headers.
     * @return array
     */
    public function format_headers($headers){
        $collect = [];
        foreach($headers as $k => $v)
            $collect[] = "$k: $v";
        return $collect;
    }

    /**
     * Create options for a stream context.
     * @param $content_type string content type, 'json', 'x-www-form-urlencoded', etc.
     * @param $method string http method, like POST, GET, etc.
     * @param $data string|array data to be sent (not used for GET requests).
     * @param $auth string|array authorization token, if a string supplied then
     * Basic auth is assumed, otherwise a one element assoc array can be supplied
     * like ['Bearer' => 'some auth key here']. Auth key will be base64 encoded
     * when $this->encode_auth is set to true. If an Authorization header is supplied,
     * it will override the auth parameter if it is present in the method call.
     * @param array $headers assoc array of additional headers and their values.
     * @return array
     */
    public function make_context_options($content_type, $method, $data = [], $auth = '', $headers = []){
        $isGet = $method == self::GET;
        if($data and is_array($data))
            $data = match ($content_type) {
                self::CT_JSON => json_encode($data),
                self::CT_FORM => http_build_query($data),
                default       => $data
            };
        if($this->cookies)
            $headers[self::Cookie] = $this->cookies;
        //$headers['Connection'] = 'keep-alive';
        //$headers['Accept']     = '*/*';
        //$headers['User-Agent'] = "Glowbug Sender";
        if($content_type)
            $headers[self::Content_Type] = $content_type;

        /// Authorization header in headers will override auth value
        if($auth && !isset($headers[self::Authorization]))
            $headers[self::Authorization] = $this->format_auth($auth);

        return [
            'http' => [
                'timeout'          => 300,
                'protocol_version' => 1.1,
                'method'           => $method,
                'header'           => $this->format_headers($headers),
                'content'          => !$isGet? $data : '',
                'ignore_errors'    => $this->ignore_post_errors
            ],
            "ssl"  => [
                "verify_peer"      => $this->verify_ssl,
                "verify_peer_name" => $this->verify_ssl,
            ]
        ];
    }

    /**
     * Create ssl context options.
     * @param $cert_path string path to the certificate file.
     * @param string $passphrase to be used for the certificate.
     * @param string $method string http method, like POST, GET, etc.
     * @param string $content_type request content type.
     * @param array $data to be sent to the remote endpoint.
     * @return resource
     */
    public function make_ssl_context_options($cert_path, $passphrase = '', $content_type = self::CT_TEXT, $method = self::GET, $data = [], $auth = '', $headers = []){
        $opts = $this->make_context_options($content_type, $method, $data, $auth, $headers);

        $opts['ssl']['local_cert'] = $cert_path;
        $opts['ssl']['passphrase'] = $passphrase;

        return stream_context_create($opts);
    }

    /**
     * Extract cookies from the response header and save them in $this->cookies field.
     * A new context will need to be created to include the extracted cookies.
     * @return string all cookies flattened in a single Cookie: request header.
     * @throws Exception
     */
    public function set_cookies(){
        if(!$this->response_headers)
            throw new Exception("No response headers present, have you made a request yet?");

        $cookies = [];
        foreach($this->response_headers as $i => $header){
            if(!$i) //skip status header
                continue;
            list($key, $value) = explode(":", $header);
            if(strtolower($key) != 'set-cookie')
                continue;
            $cookies[] = explode(";", $value)[0];
        }
        return $this->cookies = implode("; ", $cookies);
    }

    /**
     * Add cookies to the sender.
     * @param $cookies string[] assoc list of cookie keys and values.
     * @return string
     */
    public function add_cookies($cookies){
        $collect = [];
        foreach($cookies as $name => $value)
            $collect[] = "$name=$value";

        $cookies       = implode("; ", $collect);
        $this->cookies = $this->cookies? "$this->cookies; $cookies" : $cookies;
        return $this->cookies;
    }

    /**
     * Load contents of a url with a given http context.
     * @param $url string url to query.
     * @param $context resource http context array.
     * @return false|string|array response from the url, if false
     * returned then there was an error accessing the url.
     * @throws Exception if response from the url was false
     * and $this->ignore_post_errors is set to false.
     */
    public function load($url, $context){
        //suppressing warnings to avoid littering script output
        $response = @file_get_contents($url, false, $context);
        if(!isset($http_response_header))
            throw new Exception("\$http_response_header var is not present after making a request, does the host exist?", 404);

        $this->response_headers = $http_response_header;
        $this->http_status      = $http_response_header[0];
        $this->http_code        = intval(substr($this->http_status, 9, 3));

        if(!$this->ignore_post_errors && $response === false)
            throw new Exception("Got '$this->http_status' response status when calling '$url'.");
        return $this->json_decode? json_decode($response, true) : $response;
    }

    /**
     * A method for making remote requests using the PHP CURL submodule.
     * Can be handy for troubleshooting if the main method not working fsr
     * as it appears that CURL requests more resilient.
     * @param $url string remote endpoint url.
     * @param $type string content type.
     * @param $method string coonnection method, POST, GET, etc.
     * @param $data array data to send for JSON payloads.
     * @param $auth string|array see documentation for format_auth().
     * @param $headers array headers in key=>value format.
     * @return bool|string
     */
    public function load_curl($url, $type = self::CT_JSON, $method = self::POST, $data = [], $auth = '', $headers = []){
        $headers[self::Connection] = 'close';

        if($isPost = $method == self::POST)
            $headers[self::Content_Type] = $type;

        if($auth && !isset($headers[self::Authorization]))
            $headers[self::Authorization] = $this->format_auth($auth);

        $curl = curl_init();
        $opts = [
            //CURLOPT_HEADER         => true,  // display headers in standard output
            CURLOPT_RETURNTRANSFER => true,                            // return response as string
            CURLOPT_URL            => $url,                            // endpoint url
            CURLOPT_POST           => $isPost,                         // if it is a post request
            CURLOPT_HTTPHEADER     => $this->format_headers($headers), // request headers
        ];
        if($data)
            $opts[CURLOPT_POSTFIELDS] = json_encode($data); // request contents

        curl_setopt_array($curl, $opts);

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }

    /**
     * Make default curl options for a single multi request.
     * @param array $append additional opts assoc array to add.
     * @param bool $verbose whether to produce verbose output
     * from curl for debugging purposes.
     * @return array
     */
    public function make_mt_curl_opts($append = [], $verbose = false){
        $default = [
            /// return requested content as string
            CURLOPT_RETURNTRANSFER => true,
            /// return headers with response
            CURLOPT_HEADER         => true,
            CURLOPT_NOBODY         => false,
            /// max seconds wait for connect
            CURLOPT_CONNECTTIMEOUT => 10,
            /// single request timeout, if it times out
            /// the response is going to be null
            CURLOPT_TIMEOUT        => 60,
            CURLOPT_USERAGENT      => 'Curl',
            /// add bearer token
            CURLOPT_HTTPHEADER     => [],
        ];
        if($verbose){
            $default[CURLOPT_VERBOSE] = true;
            $default[CURLOPT_STDERR]  = fopen("php://output", "w");
        }

        return array_replace($default, $append);
    }

    /**
     * Split an array into chunks of specified size keeping the keys.
     * @param array $input The input array to be chunked.
     * @param int $size The size of each chunk.
     */
    public function array_chunk($input, $size){
        $i     = 0;
        $array = [];
        foreach($input as $key => $value){
            if(!isset($array[$i]))
                $array[$i] = [];

            if(count($array[$i]) >= $size)
                $i++;

            $array[$i][$key] = $value;
        }
        return $array;
    }

    /**
     * Run a series of multithreaded requests.
     *
     * @param $list array list of items to process.
     *
     * @param $cb_in callable to run on each item before call:
     * callable($item, &$opts, $id) and which is expected to
     * return an url, with the following arguments:
     *  $item – the data item to supply to the callable,
     *  $opts – curl options array that can be tweaked if needed.
     *  $id   - id of the processed element in the list.
     *
     * @param $cb_out callable to run on each response:
     * callable($item, $status, $body, $headers, $id), where:
     *  $item   - item being processed,
     *  $status - http status result after processing the item,
     *  $body   - body of the response, as a string,
     *  $id     - id of the processed element in the supplied list.
     * @param array $opts additional options for make_mt_curl_opts().
     *
     * @param $cb_batch callable to run before a batch is run:
     * callable(&$opts, $batch, $batch_id, $batch_count) where
     *  $opts        – the default curl options used for requests,
     *      can be changed by the callback (auth headers update, e.g.).
     *  $batch       – the batch that is going to be run.
     *  $batch_id    – the sequence num of the batch, starts with 0.
     *  $batch_count – the total number of batches to run.
     *
     * @param int $batch_size
     */
    public function mt_run($list, $cb_in, $cb_out = null,
        $opts = [], $cb_batch = null, $batch_size = 40){
        $multi = curl_multi_init();
        $opts  = $this->make_mt_curl_opts($opts);

        $batches    = $this->array_chunk($list, $batch_size);
        $batchCount = count($batches);
        foreach($batches as $i => $chunk){
            if($cb_batch)
                $cb_batch($opts, $chunk, $i, $batchCount);

            $batch = [];
            foreach($chunk as $id => $item){
                $url  = $cb_in($item, $opts, $id);
                $curl = $batch[$id] = curl_init($url);
                curl_setopt_array($curl, $opts);
                curl_multi_add_handle($multi, $curl);
            }

            do {
                curl_multi_exec($multi, $running);
                curl_multi_select($multi);
            } while($running);

            foreach($batch as $id => $curl){
                //$info = curl_multi_info_read($multi);
                //$err  = curl_strerror($info['result']);
                if($cb_out){
                    $status      = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                    $response    = curl_multi_getcontent($curl);
                    $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
                    $headers     = substr($response, 0, $header_size);
                    $body        = substr($response, $header_size);
                    $cb_out($list[$id], $status, $body, $headers, $id);
                }
                /// close the curl handles
                curl_multi_remove_handle($multi, $curl);
                curl_close($curl);
            }
        }
        curl_multi_close($multi);
    }


    /**
     * Make a post request to remote endpoint in json format.
     * @param $url string endpoint url.
     * @param array $data data to be sent. If an empty array supplied an empty
     * json object is sent out.
     * @param string $auth authorization string.
     * @param array $headers assoc array of headers and their values.
     * @return array|false|string
     * @throws Exception
     */
    public function send_post_json($url, $data = [], $auth = '', $headers = []){
        $data = !$data? "{}" : json_encode($data);
        $opts = $this->make_context_options(self::CT_JSON, self::POST, $data, $auth, $headers);

        return $this->load($url, stream_context_create($opts));
    }

    /**
     * Make a get request to remote endpoint.
     * @param $url string endpoint url.
     * @param array $data data to be sent.
     * @param string $auth authorization string.
     * @param array $headers assoc array of headers and their values.
     * @return array|false|string
     * @throws Exception
     */
    public function send_get($url, $data = [], $auth = '', $headers = []){
        $opts    = $this->make_context_options('', self::GET, '', $auth, $headers);
        $request = $data? "$url?" . http_build_query($data) : $url;
        return $this->load($request, stream_context_create($opts));
    }

    /**
     * Make a post request to remote endpoint in post form format.
     * @param $url string endpoint url.
     * @param array $data data to be sent.
     * @param string $auth authorization string.
     * @param array $headers assoc array of headers and their values.
     * @return mixed
     * @throws Exception
     */
    public function send_post_form($url, $data, $auth = '', $headers = []){
        $opts = $this->make_context_options(self::CT_FORM, self::POST, http_build_query($data), $auth, $headers);
        return $this->load($url, stream_context_create($opts));
    }

    public function send_get_text($url, $text, $auth = ''){
        $opts = $this->make_context_options(self::CT_TEXT, self::GET, $text, $auth);
        return $this->load($url, stream_context_create($opts));
    }

    public function send_post_text($url, $text, $auth = ''){
        $opts = $this->make_context_options(self::CT_TEXT, self::POST, $text, $auth);
        return $this->load($url, stream_context_create($opts));
    }

    /**
     * Create a field to submit to a remote point as a part of multipart/form-data.
     * @param $name string POST field name.
     * @param $contentType string content type of the field like "text/plain".
     * @param $value string value of the field, can supply binary data.
     * @param string $filename string if supplying binary data this is the
     * file name that the server should treat the submitted value as.
     * @return string text display of the form field.
     */
    public function make_field($name, $contentType, $value, $filename = ''){
        $filename = $filename? "; filename=\"$filename\"" : "";
        return <<< field
Content-Disposition: form-data; name="$name"$filename
Content-Type: $contentType

$value
field;
    }

    /**
     * Send multipart form data to a remote endpoint.
     * //todo: need to test this properly
     * @param $url string remote endpoint to connect to.
     * @param $fields string[] fields to be sent out.
     * @param string $auth
     * @return false|string
     */
    public function send_post_multipart($url, $fields, $auth = ''){
        $boundary  = base64_encode(random_bytes(20));
        $fn        = function($field) use ($boundary){
            return "--$boundary\n$field";
        };
        $content   = array_map($fn, $fields);
        $content[] = "--$boundary--";
        $content   = implode("\n", $content);

        $opts = $this->make_context_options("multipart/form-data; boundary=$boundary", self::POST, $content, $auth);
        return $this->load($url, stream_context_create($opts));
    }
}
