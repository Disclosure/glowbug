<?php

namespace glowbug\misc;

use Exception;

/**
 * Class for dealing with MS Office 365 services communication.
 */
class Graph extends Sender {

    /**
     * @var string AD app TenantID.
     */
    public $tenant_id;

    /**
     * @var string AD app ClienID.
     */
    public $client_id;

    /**
     * @var string AD app secret value (not ID).
     */
    public $secret;

    /**
     * @var string token returned by auth procedure.
     */
    public $token;

    /**
     * @var string base url for interaction with the service.
     */
    public $base_url = 'https://graph.microsoft.com/v1.0';

    /**
     * Some notes on setting up access in the Office 365 admin dash.
     * Create a new app at this url: https://aad.portal.azure.com/#view/Microsoft_AAD_IAM/ActiveDirectoryMenuBlade/~/RegisteredApps
     *
     * The app needs to be set up with credentials after creation on the
     * appropriate screen, the three types needed by the class to are:
     *
     * Client ID, Tenant ID, Secret Value
     *
     * Those credentials have an expiry date and should be reviewed/recreated.
     *
     * Navigate to App Permissions section after credentials creation.
     * Select following APPLICATION (not Delegated) permissions in Graph section:
     *
     * User.Read.All to list users and get their ids. Those also can be found
     * in admin dashboard, those are shown as Object ID in user properties.
     *
     * Mail.ReadWrite in case reading mail of a user is required.
     *
     * Mail.Send is needed to be able to send email on behalf of a user. Needs a
     * user id (those can be read using User.Read.All permission as per above).
     * User id looks similar to 8d3e3109-f5a4-42b9-8e8c-e8ca7d84e658 and is needed
     * when making queries to their mail inboxes.
     *
     * Once all the permissions are selected, those need to be granted to the
     * application by the admin, there is a inconspicuous button with a check
     * mark on the permission display screen. Configuring permissions and looking
     * for all the settings can be hairy on the first time but overall it appears
     * to be the easiest way to send email using Microsoft's services.
     *
     * @param $tenant_id
     * @param $client_id
     * @param $secret
     * @throws Exception
     */
    public function __construct($tenant_id, $client_id, $secret){
        $this->tenant_id = $tenant_id;
        $this->client_id = $client_id;
        $this->secret    = $secret;
        $this->token     = $this->get_token();

        $this->encode_auth = false;
        parent::__construct();
    }

    /**
     * Get auth token from the remote service.
     * @return array Bearer Authorisation header.
     * @throws Exception
     */
    protected function get_token(){
        $data = [
            'client_id'     => $this->client_id,
            'scope'         => "https://graph.microsoft.com/.default",
            'client_secret' => $this->secret,
            'grant_type'    => 'client_credentials',
        ];
        return [self::Bearer => $this->send_post_form("https://login.microsoftonline.com/$this->tenant_id/oauth2/v2.0/token", $data)['access_token']];
    }

    /**
     * Make a validated GET request.
     * @param $endpoint string url slug after the base url.
     * @param $data array optional get vars list.
     * @return array|false
     * @throws Exception
     */
    public function get($endpoint, $data = []){
        return $this->send_get("$this->base_url/$endpoint", $data, $this->token);
    }

    /**
     * Make a validated POST request.
     * @param $endpoint string url slug after the base url.
     * @param $data array optional post vars list.
     * @return array|false
     * @throws Exception
     */
    public function post($endpoint, $data = []){
        return $this->send_post_json("$this->base_url/$endpoint", $data, $this->token);
    }

    /**
     * Make a validated DELETE request.
     * @param $endpoint string url slug after the base url.
     * @return array|false
     * @throws Exception
     */
    public function delete($endpoint){
        $opts = $this->make_context_options('', self::DELETE, '', $this->token);
        return $this->load("$this->base_url/$endpoint", stream_context_create($opts));
    }

    /**
     * List users of this organisation.
     * @return array|false
     * @throws Exception
     */
    public function get_users(){
        return $this->get('users');
    }
}
