<?php

namespace glowbug\misc;

/**
 * Core Telegram bot functionality implementation.
 */
class TgBot extends Sender {

    /**
     * @var string url endpoint to which to send back responses from the bot,
     * similar to https://api.telegram.org/bot1859905040:AAG-3H53IcogaFKeL6XPNUGiRFO3DimL2U4
     * Should be typically specified in subclasses.
     */
    public $api_url = "";

    /**
     * @var array the incoming message.
     */
    public $message = [];

    /**
     * @var string|int telegram user id.
     */
    public $uid = '';

    /**
     * @var string user's telegram username.
     */
    public $username = '';

    /**
     * @var string user's first name.
     */
    public $first_name = '';

    /**
     * @var bool whether the user is a bot.
     */
    public $user_is_bot = false;

    /**
     * @var string|int id of the incoming message's chat.
     */
    public $chat_id = '';

    /**
     * @var string|int id of the incoming message bot can respond to.
     */
    public $message_id = '';

    /**
     * @var string text content of the incoming message.
     */
    public $text = '';

    /**
     * @var bool for testing purposes $this->request() will format
     * response output into indented json if set to true.
     */
    public $return_formatted_json = false;

    /**
     * @var array from user params.
     */
    public $from = [];

    /**
     * @var string bot command issued, 0th element is the command,
     * the rest are params.
     */
    public $command = [];

    /**
     * Extract telegram command with a single parameter from string.
     * Will split on newlines and underscores.
     * @param $string
     * @return string[]
     */
    public function get_command($string){
        if(!str_starts_with($string, '/'))
            return [];
        return preg_split('/\s|_/', substr($string, 1), 2, PREG_SPLIT_NO_EMPTY);
    }

    public function __construct($message = []){
        if($message){
            $this->message = $message;
            $this->text    = trim($message['text']);
            $this->command = $this->get_command($this->text);

            $mf                = $message['from'];
            $this->from        = $mf;
            $this->uid         = $mf['id'];
            $this->user_is_bot = $mf['is_bot'];
            $this->username    = $mf['username'];
            $this->first_name  = $mf['first_name'];

            $this->chat_id    = $message['chat']['id'];
            $this->message_id = $message['message_id'];
        }

        parent::__construct();
    }

    public function encode($array){
        return $this->return_formatted_json?
            json_encode($array, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE) :
            json_encode($array);
    }

    /**
     * Make a request back to Telegram API without having to be called by an external entity.
     * @param $method string endpoint method to call.
     * @param array $params call parameters.
     * @return false|string
     */
    public function request($method, $params = []){
        $params['method'] = $method;
        $result           = $this->send_post_json("$this->api_url/$method", $params);
        return $this->encode($result);
    }

    /**
     * Respond to an incoming call.
     * @param $method string endpoint method to call.
     * @param array $params call parameters.
     * @return bool
     */
    public function respond($method, $params = []){
        $params["method"] = $method;

        $payload = $this->encode($params);
        header('Content-Type: application/json');
        header('Content-Length: ' . strlen($payload));
        echo $payload;

        return "";
    }

    /**
     * Respond to an incoming call with a text message.
     * @param $text string message to respond with.
     * @return bool
     */
    public function reply($text){
        return $this->respond("sendMessage", [
            'chat_id'    => $this->chat_id,
            'text'       => $text,
            'parse_mode' => 'HTML',
        ]);
    }

    public function set_webhook($url, $drop_updates = true){
        return $this->request('setWebhook', [
            'url'                  => $url,
            'drop_pending_updates' => $drop_updates,
        ]);
    }

    public function get_webhook_info(){
        return $this->request('getWebhookInfo');
    }
}
