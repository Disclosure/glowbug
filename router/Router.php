<?php

namespace glowbug\router;

use Exception, ReflectionClass, ReflectionException;

/**
 * Class Router is used as base class for specific routers.
 */
class Router {
    /**
     * @var string subdirectory of the domain where project is hosted.
     * Typically is useful for PageRouters which use that to construct
     * navigation and asset loading urls.
     * Should NOT include a trailing slash.
     */
    public string $root_url = '';

    /**
     * @var ?string whether the logged user is admin, may contain an email
     * address when set, or the admin's username.
     */
    public ?string $admin = '';

    /**
     * @var ?string whether the logged user is sub-admin, may contain an email
     * address when set, or the sub-admin's username. It can be used for giving
     * admin access to a subset of models.
     */
    public ?string $sub_admin = '';

    /**
     * @var ?string whether the logged user is a sub user, meaning it belongs to a user but has a subset of rights of the full user.
     */
    public ?string $sub_user = '';

    /**
     * Whether the logged-in user has any admin rights at all, meaning whether
     * the user is logged in as either admin, or sub-admin.
     * @return string
     */
    public function can_admin(){
        if($this->admin)
            return $this->admin;
        if($this->sub_admin)
            return $this->sub_admin;
        return '';
    }

    /**
     * Current router action/page.
     * @var string
     */
    public string $action;

    /**
     * @var string method prefixes for remotely accessible endpoints.
     */
    public string $action_prefix;

    /**
     * @var string default action when action block is empty in url.
     * Default action may contain dashes in the name (unlike public actions),
     * as those will be replaced with underscores at the dispatch time.
     */
    public string $default_action = 'main';

    /**
     * @var array all endpoint call arguments in array.
     */
    public array $args = [];

    /**
     * @var array arguments that have been validated.
     */
    public array $valid_args = [];

    /**
     * @var int current index of args list reader.
     */
    private int $arg_pos = 0;

    /**
     * @var array|string actions that do not require a valid login.
     * If the array is empty, all router endpoints considered public.
     * All elements should have match the router method names without
     * the action prefix, e.g. for a page /new-page in a page router,
     * router action will be 'new_page' and method name page_new_page().
     * If that action is public then $this->public_actions = ['new_page'];
     */
    public $public_actions = [];

    /**
     * @var array list of actions that do not cause a session refresh.
     * Can be used when an automated action performed by a script on the
     * frontend that does not count for manual intervention, for example
     * system may be timing how long a DB record was open on screen,
     * however only manual interaction would extend the session.
     */
    public $no_refresh_actions = [];

    /**
     * @var bool whether running in CLI mode.
     */
    public bool $cli;

    /**
     * @var int default timeout in seconds on live envs.
     */
    public int $live_wait = 4;

    /**
     * @var int session expire time in seconds.
     */
    public int $expire_time = 3600;

    /**
     * @var bool whether to log exceptions caught in route() method
     * to the main php error log.
     */
    public bool $log_exceptions = true;

    /**
     * @var string main admin email address, typically is used
     * to forward caught exceptions to.
     */
    public string $service_email = '';

    /**
     * Process an exception thrown during route() calls. Will log the
     * exception to the main php error log if $this->log_exceptions is
     * set to true, if $this->admin_email provided, will additionally
     * forward the exception in an email message to that address.
     * @param $ex Exception exception to process.
     */
    public function process_exception($ex){
        if($this->log_exceptions)
            error_log($ex);
        if($this->service_email){
            $subject = "Exception " . $ex->getCode() . " caught on " . $_SERVER['HTTP_HOST'];
            $message = $ex->getMessage() . "\n\n" . $ex->getTraceAsString();
            mail($this->service_email, $subject, $message, ['From' => $this->service_email]);
        }
    }

    /**
     * @var array used to collect output headers.
     * Assoc array, where key is the name of the header,
     * the default value is "HTTP/1.0 200 OK"
     */
    public array $headers = ["HTTP/1.0" => "200 OK"];

    /**
     * A shortcut for _queue_header("HTTP/1.0", "$code $message")_.
     * @param $code int|string http status code.
     * @param $message string response message.
     */
    protected function set_status($code, $message){
        $this->headers["HTTP/1.0"] = "$code $message";
    }

    /**
     * Queue a header for output, for example:
     * _queue_header('Content-Length', strlen($result)_
     * @param string $name header name.
     * @param string $value header value.
     */
    public function queue_header(string $name, string $value){
        $this->headers[$name] = $value;
    }

    /**
     * Emit queued headers. Should be done just before
     * the response is echoed back to the client.
     * @return array
     */
    public function emit_headers(){
        foreach($this->headers as $key => $value)
            header($key == "HTTP/1.0"? "$key $value" : "$key: $value");
        return $this->headers;
    }

    /**
     * If live wait for a bit before page opens. That is to
     * prevent url_slug brute forcing.
     * @param bool $do_wait whether the env is live.
     * @param null|int $seconds
     */
    public function wait_if(bool $do_wait, $seconds = null){
        if($do_wait)
            sleep($seconds ?? $this->live_wait);
    }

    /**
     * @var null|bool cached value of the $this->is_public() call.
     */
    private $_is_public = null;

    /**
     * Whether the router action is available publicly.
     * @return bool
     */
    public function is_public(){
        if($this->_is_public !== null)
            return $this->_is_public;

        return $this->_is_public = !$this->public_actions || in_array($this->action, $this->public_actions);
    }

    /**
     * @var mixed current user logged in ID, null when not logged in.
     * Will be set by check_login().
     */
    public $uid = null;

    public function __construct(string $url = ''){
        $this->cli = php_sapi_name() == 'cli';
    }

    /**
     * Initialise the router. Will set the action and its args.
     * @param $action string
     * @param $args array
     * @return $this
     */
    public function init($action = '', $args = []){
        $this->args   = $args;
        $this->action = $action?: $this->default_action;
        return $this;
    }

    /// Below are core login tiers in case it is necessary
    /// to additionally qualify the current user login.
    /// These levels should be identified by setting appropriate
    /// session vars to non-false values. It is assumed that only
    /// one level would be set at a time.
    /// For example, to specify that the current user is admin one
    /// could do as follows:
    /// $this->sesh(self::ADMIN, 'admin@email.com');
    const ADMIN = 'admin';           // user has admin privileges
    const SUB_ADMIN = 'sub_admin';   // user has some admin privileges
    const PLAIN_USER = 'plain_user'; // user without any admin privileges

    /**
     * Start and configure a PHP session.
     * @param bool $cs_cookies if to allow cross site cookie access.
     * @return mixed|null current logged in user uid.
     * @throws Exception
     */
    public function setup_session($cs_cookies = false){
        if($this->cli)
            return null;

        session_set_cookie_params([
            'SameSite' => $cs_cookies? 'None' : 'Strict',
            'Secure'   => $_SERVER['HTTPS'] ?? false,
        ]);

        session_start();
        if($this->get('logout'))
            return $this->logout();

        if($this->uid = $this->is_logged_in()){
            $time      = time();
            $notSameIp = $this->sesh('ip') != $_SERVER['REMOTE_ADDR'];
            $expired   = ($time - $this->sesh('time')) > $this->expire_time;

            if($notSameIp || $expired)
                return $this->logout();
            elseif(!in_array($this->action, $this->no_refresh_actions))
                $this->sesh('time', $time);

            foreach([self::ADMIN, self::SUB_ADMIN, self::PLAIN_USER] as $field)
                if($value = $this->sesh($field))
                    $this->$field = $value;
        }

        $this->verify_csrf = true;

        return $this->uid;
    }

    /**
     * Get the router method to call according to the current request method.
     * For example, for a potential candidate method for a POST request method
     * a valid method name would be "$action\_prefix\_$action\_post'". If
     * a specialized method does not exist, will attempt to find common method
     * "$action_prefix_$action" name.
     * @return string|string[]
     */
    public function get_method(){
        $method  = $this->action_prefix . $this->action;
        $special = $method . "_" . strtolower($_SERVER['REQUEST_METHOD']);
        if(method_exists($this, $special))
            $method = $special;
        else if(!method_exists($this, $method))
            $method = '';
        return $method;
    }

    /**
     * Force current session destruction and starting a new one.
     * @return null current user id set to null.
     */
    public function logout(){
        session_destroy();
        session_start();
        return $this->uid = null;
    }

    /**
     * Code in subclass to run before endpoint method call but after
     * its parameters have been validated. If it returns anything but
     * null, the returned value will be sent back to client. Useful
     * for implementing router specific validation and checking.
     */
    public function before(){
        return null;
    }

    /**
     * Code in subclass to run after successful endpoint method call.
     */
    public function after(){
    }

    /**
     * Get argument at a specific position, and optionally assign it to a field.
     * If argument does not exist will return null, however null won't be assigned
     * to a field even if that has been specified which will allow it to keep its
     * default value.
     * @param $pos int argument position. If not supplied will use $this->arg_pos
     * position counter that will be incremented after a read.
     * @param string $set_if_present_to if argument present at position,
     * object field to assign it to.
     * @return string|null will return
     */
    public function get_arg(int $pos = -1, string $set_if_present_to = ''){
        if($pos < 0)
            $pos = $this->arg_pos++;
        $arg = $this->args[$pos] ?? null;
        if($set_if_present_to && $arg !== null)
            $this->$set_if_present_to = $arg;
        return $arg;
    }

    /**
     * Check if the action method call is valid with the params supplied in url.
     * @param string $method name of the ajax method.
     * @param int $fail_on_extra_args error code to fail with if more args supplied
     * than the method accepts, useful for page routing where an extra arg would
     * point to a non-existent page.
     * @return array validated parameters for the method call.
     * @throws ReflectionException
     * @noinspection PhpConditionAlreadyCheckedInspection
     */
    public function validate_method_call(string $method, int $fail_on_extra_args = 0){
        $refClass  = new ReflectionClass($this);
        $refMethod = $refClass->getMethod($method);
        $params    = $refMethod->getParameters();

        if($fail_on_extra_args)
            if($parCount = count($params) < $argsCount = count($this->args))
                throw new RouterException("Parameter count '$parCount' exceeds '$method' method argument count '$argsCount'.", $fail_on_extra_args);

        $args = [];
        foreach($params as $param){
            $arg = $this->get_arg();
            if($arg === null){
                if(!$param->isOptional())
                    throw new RouterException("Compulsory parameter '$param->name' in endpoint '$this->action' hasn't been supplied.", 400);
                break;
            }
            switch($param->getType()){
                case 'int':
                case 'integer':
                    $num = is_numeric($arg);
                    if($num)
                        $this->valid_args[$param->name] = $args[] = intval($arg);
                    else throw new RouterException("Numeric parameter '$param->name' in method '$method' should be a number.", 400);
                    break;
                default: //assuming a string
                    if($param->name == 'model')
                        $arg = ucfirst($arg);
                    $this->valid_args[$param->name] = $args[] = $arg;
            }
        }

        return $args;
    }

    /**
     * Generate pdf from html supplied. Requires _wkhtmltopdf_ installed.
     * It assumes c:/temp/ as the temporary file folder on windows.
     * @param $html string to generate pdf from.
     * @param string $download if supplied will emit pdf download
     * headers with $download being the filename of the pdf.
     * @return string pdf file content.
     */
    public function make_pdf(string $html, string $download = ''){
        $win      = defined('PHP_WINDOWS_VERSION_MAJOR');
        $filename = uniqid("pdfgen-");

        chdir($win? "c:/temp/" : "/tmp/");
        file_put_contents("$filename.html", $html);
        exec("wkhtmltopdf --print-media-type --no-outline --margin-top 20mm --margin-bottom 20mm --margin-left 30mm --margin-right 30mm $filename.html $filename.pdf");
        $pdf = file_get_contents($fname = "$filename.pdf");
        if($download){
            $this->queue_header('Content-Type', 'application/pdf');
            $this->queue_header('Content-Length', filesize($fname));
            $this->queue_header('Content-Disposition', "attachment;filename=$download.pdf");
        }
        exec($win? "del $filename*" : "rm $filename*");
        return $pdf;
    }

    /**
     * Return csv download.
     * @param string[][] $csv csv to output.
     * @param string $filename download filename including extension,
     * will emit headers if supplied.
     * @return string
     */
    public function csv($csv, string $filename = ''){
        $name = "/tmp/csv-" . random_string() . ".csv";
        $file = fopen($name, "w");
        foreach($csv as $line)
            fputcsv($file, $line);
        fclose($file);
        $csv = file_get_contents($name);

        $this->queue_header('Content-Type', 'text/csv; charset=UTF-8');
        $this->queue_header('Content-Length', strlen($csv));
        $this->queue_header('Content-Disposition', "attachment;filename=$filename");

        return $csv;
    }

    /**
     * Shorthand for POST var extraction.
     * @param $var string name.
     * @param bool $escape_html whether to escape html tags in content.
     * @return mixed will return null if not set.
     */
    public function post($var, $escape_html = true){
        if(isset($_POST[$var])){
            $val = $_POST[$var];
            ///todo: might make more sense to escape array values
            if(is_array($val))
                return $val;
            return $escape_html? htmlentities($val) : $val;
        }
        return null;
    }

    /**
     * Shorthand for GET var extraction.
     * @param $var string name
     * @return mixed|null will return null if not set.
     */
    public function get(string $var){
        return $_GET[$var] ?? null;
    }

    /**
     * Get variable name value regardless of whether it is GET or POST var.
     * Post vars take precedence over get vars.
     * @param $var string name.
     * @return mixed|null
     */
    public function data($var){
        if($post = $this->post($var))
            return $post;
        return $this->get($var);
    }

    /**
     * Shorthand for dealing with session variables.
     * @param $var_name string of the session variable.
     * @param mixed|null $set_to value to set the variable to,
     * if null then assumes that reading a variable.
     * @return string|null will return null if var is unset.
     */
    public function sesh(string $var_name, ?string $set_to = null){
        if(session_status() == PHP_SESSION_NONE)
            throw new Exception('No active session running.');
        if($set_to === null)
            return $_SESSION[$var_name] ?? null;
        $_SESSION[$var_name] = $set_to;
        return $set_to;
    }

    /**
     * Check if the session has a logged in user assigned to it.
     * @return string|int|null uid of the user, or null if not logged in.
     */
    public function is_logged_in(){
        return $this->sesh('uid');
    }

    /**
     * Set uid field with logged in user id and update session with it.
     * @param string|int $uid user id that has logged in.
     * @param string $admin whether the user is admin, will contain an email if set.
     * @param string $sub_admin whether user has sub-admin rights, will contain an email if set.
     * @param string $sub_user whether the logged in user is a limited rights user.
     * @return string|int
     * @throws Exception
     */
    public function set_login($uid, $admin = '', $sub_admin = '', $sub_user = ''){
        $this->sesh('uid', $uid);
        $this->sesh('time', time());
        $this->sesh('ip', $_SERVER['REMOTE_ADDR']);
        if($admin)
            $this->sesh(self::ADMIN, $admin);
        if($sub_admin)
            $this->sesh(self::SUB_ADMIN, $sub_admin);
        if($sub_user)
            $this->sesh(self::PLAIN_USER, $sub_user);
        return $this->uid = $uid;
    }

    /**
     * Set csrf token on the session. Will return an empty string when in CLI.
     * @return mixed
     * @throws Exception
     */
    public function set_csrf(){
        if($this->cli || !$this->verify_csrf)
            return '';
        if(!$csrf = $this->sesh('csrf'))
            $csrf = $this->sesh('csrf', base64_encode(random_bytes(12)));
        return $csrf;
    }

    /**
     * @var bool whether to check csrf token. Can be used to disable
     * form submission csrf verification on services where cookies
     * cannot be used and other means to validate the call should be
     * used, e.g. APIs. Will automatically set to true after calling
     * $this->setup_session().
     */
    public bool $verify_csrf = false;

    /**
     * Check if the csrf supplied matches the csrf on the session.
     * If running in CLI will always return true. Will also return
     * true if $this->verify_csrf is set to false or if method is GET.
     * @return bool
     */
    public function check_csrf(){
        if(!$this->verify_csrf || $this->cli || $_SERVER['REQUEST_METHOD'] == 'GET')
            return true;
        return $this->post('csrf', false) == $this->sesh('csrf');
    }

    /**
     * Return request's headers and body.
     * @param string $include_body whether to include request body.
     * @return string entire request as a string.
     */
    public function raw_request($include_body = ''){
        $headers = ["$_SERVER[REQUEST_METHOD] $_SERVER[REQUEST_URI] $_SERVER[SERVER_PROTOCOL]"];
        foreach($_SERVER as $key => $value){
            if(strpos($key, 'HTTP_') === 0){
                $index = str_replace('_', ' ', strtolower(substr($key, 5)));
                $index = str_replace(' ', '-', ucwords($index));

                $headers[] = "$index: $value";
            }
        }
        if($include_body)
            $headers[] = "\n" . file_get_contents("php://input");

        return implode("\n", $headers);
    }

    /**
     * Add xdebug cookie. Needs an active session.
     */
    public function enable_xdebug(){
        setcookie('XDEBUG_SESSION', "DEVELOPMENT", [
            'samesite' => "Lax",
            'path'     => '/'
        ]);
    }
}
