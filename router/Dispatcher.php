<?php

namespace glowbug\router;

class Dispatcher {
    /**
     * @var array assoc array of router names and their instances.
     */
    public $routers;

    /**
     * @var string root url of the application, for example '/myapp/'.
     * If the app is hosted in the root of the domain, this should be an empty string.
     */
    public $root_url;

    /**
     * @var array[] assoc array of router names and their constructor args,
     * see the constructor for more info about how dispatch works.
     */
    public $router_args;

    /**
     * @var string namespace of the application routers, for example 'routers\\'.
     * The class autoloader should be able to find the routers in this namespace.
     */
    public $router_namespace;

    /**
     * @param $router_args array[] available router names and their constructor args.
     * If no arguments consumed, an empty array should be submitted still
     * as the router names in the args array keys are the routers than can
     * be instantiated. Example dispatch:
     *
     * $db = new DB;
     *
     * $router = new Dispatcher([
     * 'MainRouter'    => ['1.15', $db],
     * 'AjaxRouter'    => [$db]
     * ], 'routers\\');
     *
     * $router->route();
     *
     * @param $router_namespace string if application routers reside in a namespace other
     * than global (\Ajax e.g.), specify it here, for example for 'routers\Ajax'
     * router use 'routers\\'  as the argument value.
     *
     * @param $root_url string subdirectory of the domain where project is hosted.
     * Should NOT include a trailing slash.
     */
    public function __construct($router_args, $router_namespace = '', $root_url = ''){
        $this->root_url         = $root_url;
        $this->router_args      = $router_args;
        $this->router_namespace = $router_namespace;
    }

    /**
     * Split request url into / denoted blocks.
     * @param string $url request url to parse.
     * @return array|false|string[]
     */
    public function split($url){
        if($this->root_url)
            $url = str_replace($this->root_url, '', $url);
        $qmark = explode('?', $url)[0]; // todo: need to handle get vars
        return preg_split('/\//', $qmark, -1, PREG_SPLIT_NO_EMPTY);
    }

    /**
     * Initialise and return a router instance for the request url.
     * Routing is performed by splitting the url into blocks and matching
     * the first block to a router name, while the rest map to the router
     * action (method) and its arguments.
     *
     * @param $url string request url. The first block of the url
     * denotes the name of the router class to instantiate.
     *
     * @param $default_router string default router name if not
     * specified in the first url block.
     *
     * @return mixed an instance of a Router subclass.
     */
    public function dispatch($url, $default_router = 'Main'){
        $blocks = self::split($url);
        if($section = $blocks[0] ?? ''){
            /// uc first block to match a router class names which are capitalised
            $section = ucfirst(str_replace('-', '_', $section));
            if(key_exists($section, $this->router_args))
                unset($blocks[0]); // remove router name from url blocks
            else $section = $default_router;
        } else $section = $default_router;

        /// instantiate router if not already done
        /** @var Router $router */
        if(!$router = $this->routers[$section] ?? null){
            $class  = $this->router_namespace . $section;
            $router = new $class(...$this->router_args[$section]);

            $router->root_url        = $this->root_url;
            $this->routers[$section] = $router;
        }

        $action = $blocks[array_key_first($blocks)] ?? '';
        $action = str_replace('-', '_', $action);
        $args   = array_slice($blocks, 1);
        $router->init($action, $args);

        return $router;
    }
}
