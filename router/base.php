<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
    <title><?= $this->page_title ?></title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="initial-scale=1"/>
    <!--    <link rel="shortcut icon" href="/images/favicon.ico" />-->
    <?php
    $this->load_css('style');
    $this->load_script('script');
    $this->load_script('vue', true);
    ?>
    <script>const csrf = '<?= $this->set_csrf() ?>';</script>
</head>
<body class="<?= $this->body_class ?>">
<?php

require $this->get_template();

?>
</body>
</html>
