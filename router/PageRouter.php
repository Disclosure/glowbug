<?php

namespace glowbug\router;

use Exception;
use Throwable;

/**
 * Class PageRouter used for dispatching page calls from the client.
 */
class PageRouter extends Router {

    public string $base_layout = 'base';
    public string $inner_layout = '';
    public string $page_title = '';

    //having the below separately will allow
    //to tweak error pages from subclasses
    public string $err_message = '';
    public string $err_page = 'error';
    public string $err_base_layout = 'base';
    public string $err_inner_layout = '';

    public string $ver;                    //version of the js and css files
    public string $web;                    //web files filesystem root

    /**
     * @var string url location of website assets, like scripts, images, css.
     * If empty then the root url is assumed, use '/' if assets are hosted
     * on the domain root.
     */
    public string $asset_url = '';
    public string $js_url = "js";          //js folder  within the asset url
    public string $img_url = "img";        //img folder within the asset url
    public string $css_url = "css";        //css folder within the asset url

    public string $pages_dir = 'pages';     //folder with page   templates in web root
    public string $layouts_dir = 'layouts'; //folder with layout templates in web root

    public string $action_prefix = "page_";

    public string $content = '';            //if supplied should override the template page load

    /**
     * PageRouter constructor. Assign base js, img and css url slugs when needed
     * before invoking the constructor as those will be updated with the asset url.
     * @param string $ver script version to be set in html.
     * @param string $web filesystem root for web files (layouts, pages, etc).
     * @param string $root_url
     */
    public function __construct(string $ver, string $web, string $root_url = ''){
        parent::__construct();
        $this->ver      = $ver;
        $this->web      = $web;
        $this->root_url = $root_url;

        if(!$this->asset_url)
            $this->asset_url = $this->root_url . "/";

        if($this->asset_url and !str_ends_with($this->asset_url, "/"))
            $this->asset_url .= "/";

        $this->js_url  = $this->asset_url . $this->js_url;
        $this->img_url = $this->asset_url . $this->img_url;
        $this->css_url = $this->asset_url . $this->css_url;

        if(!$this->cli)
            $this->queue_header('Content-Type', 'text/html; charset=UTF-8');
    }

    /**
     * Route web page calls.
     * @return string
     * @throws Exception
     */
    public function route(){
        if(!$this->is_public() and !$this->uid)
            $this->action = 'login';

        if($method = $this->get_method())
            try {
                $params = $this->validate_method_call($method, 404);
                if(!$result = $this->before()){
                    $result = $this->$method(...$params);
                    $this->after();
                }
            } catch(RouterException $ex) {
                $result = $ex->is_404()? $this->error_404() : $this->error($ex->getCode(), $ex->getMessage());
            } catch(Throwable $ex) {
                $result = $this->error(500, $ex->getMessage());
                $this->process_exception($ex);
            }
        else $result = $this->error_404();

        if(!$this->cli){
            $this->queue_header('Content-Length', strlen($result));
            $this->emit_headers();
            echo $result;
        }
        return $result;
    }

    /**
     * Check if the current page slug selected. Typically is needed when
     * need to add a selected class to a menu item for a page with a url
     * slug that matches page's endpoint action. The tricky part is when
     * the page action is default in which case we would be comparing it
     * to an empty slug but in that case it should equate default_action.
     * @param string $slug page slug to compare the current action to.
     * @return bool
     */
    public function is_current_action(string $slug){
        if($slug)
            return $this->action == $slug;
        return $this->action == $this->default_action;
    }

    /**
     * Get the template path to load within the base layout.
     * @param bool $page when false then the inner layout is loaded,
     * when true, the action page will be loaded. Usage example:
     * in base layout:
     *      <?php require $this->get_template(); ?>
     * in inner layout:
     *      <?php require $this->get_template(true); ?>
     * Alternatively there may be a check for output presence in
     * $this->content variable and displaying that instead of page template:
     * <?php
     *  if($this->content)
     *      echo $this->content;
     *  else require $this->get_template(true);
     * ?>
     * @return string
     */
    public function get_template(bool $page = false){
        if($page || !$this->inner_layout){
            $action = str_replace("_", "-", $this->action);
            return "$this->web/$this->pages_dir/$action.php";
        }
        return "$this->web/$this->layouts_dir/$this->inner_layout.php";
    }

    /**
     * Generate page html from the combination of layouts and the page.
     * @param string $page name of the page to override the default one.
     * @return string
     */
    public function html(string $page = ''){
        if($page)
            $this->action = $page;
        ob_start();
        require "$this->web/$this->layouts_dir/$this->base_layout.php";
        $html = ob_get_clean();
        return $html;
    }

    /**
     * Generate a pdf from layouts supplied and send it to client.
     * Similar to html() only that sends pdf file contents instead
     * of html markup. If filename argument is supplied, will
     * emit file download headers, otherwise one would be required
     * to do it manually.
     *
     * @param $filename string pdf download file name. Will not
     * emit download headers if $filename not supplied.
     * @param string $page name of the page to override default.
     *
     * @return string pdf string content.
     */
    public function pdf(string $filename = '', string $page = ''){
        return $this->make_pdf($this->html($page), $filename);
    }

    /**
     * Produce 404 error output.
     * @param string $message error message to send.
     * @return string
     */
    public function error_404(string $message = "Page not found"){
        return $this->error(404, $message);
    }

    /**
     * Produce 404 error output.
     * @param string $message error message to send.
     * @return string
     */
    public function not_found(string $message = "Page not found"){
        return $this->error(404, $message);
    }

    /**
     * Produce error output.
     * @param int $code error code.
     * @param string $message error message to send.
     * @param $page_title ?string optional error page title.
     * @return string
     */
    public function error(int $code, string $message, ?string $page_title = null){
        $this->action       = $this->err_page;
        $this->inner_layout = $this->err_inner_layout;
        $this->base_layout  = $this->err_base_layout;
        $this->page_title   = $page_title ?? "$code Error";
        $this->err_message  = $message;

        $this->set_status($code, $message);
        return $this->html();
    }

    /**
     * Generate and echo script tag html.
     * @param $name string name of the script.
     * @param bool $lib whether script is located in libs/.
     * @param bool $min whether to load minimized script.
     * @param bool $force whether to force load the script.
     */
    public function load_script(string $name, $lib = false, $min = false, $force = false){
        $libs = $lib? "libs/" : '';
        $min  = $min? '.min' : '';
        $ver  = $lib? '' : "?v=$this->ver";
        if($force){
            $rnd = random_string(5);
            $ver = $ver? "$ver&f=$rnd" : "?f=$rnd";
        }
        echo "<script src='$this->js_url/$libs$name$min.js$ver'></script>\n";
    }

    /**
     * Set the csrf script tag on page. Will emit empty string if
     * $this->verify_csrf is set to false.
     * @return string
     * @throws Exception
     */
    public function csrf_script(){
        if(!$this->verify_csrf)
            return '';
        $csrf = $this->set_csrf();
        echo "<script>let csrf = '$csrf';</script>";
    }

    /**
     * Generate a js script tag to load a vue component and load a vue template.
     * @param $name string name of the component (the same for both js and template).
     * @param bool $min whether to load minimized script.
     */
    public function load_comp(string $name, $min = false){
        $this->load_script("comps/$name", false, $min);
        require "$this->web/public/$this->js_url/comps/$name.php";
    }

    /**
     * Generate link tag to load a css script.
     * @param string $name name of the css script.
     * @param bool $min whether to load minimized script.
     */
    public function load_css(string $name = 'style', $min = false){
        $min = $min? '.min' : '';
        echo "<link rel='stylesheet' href='$this->css_url/$name$min.css?v=$this->ver'/>\n";
    }
}
