<?php

namespace glowbug\router;

use Exception, Throwable;

/**
 * Class ActionRouter is used for handling ajax calls.
 */
class ActionRouter extends Router {

    public string $action_prefix = "ajax_";

    public bool $pretty_print = false;

    /**
     * Route ajax endpoint calls.
     * @return array|string
     * @throws Exception
     */
    public function route(){
        $respond = function($result){
            if(!$this->cli){
                if(is_array($result)){
                    $content = 'application/json';
                    $pretty  = $this->pretty_print? JSON_PRETTY_PRINT : 0;
                    $result  = json_encode($result, $pretty);
                } else $content = 'text/plain';
                $this->queue_header('Content-Type', $content);
                $this->queue_header('Content-Length', strlen($result));
                $this->emit_headers();
                echo $result;
            }
            return $result;
        };

        if(!$this->action){
            $result = $this->not_found("No action supplied.");
            return $respond($result);
        }
        if(!$this->is_public() && !$this->uid){
            $result = $this->err("Need to be logged in to access.", 403);
            return $respond($result);
        }
        if(($_SERVER['HTTP_CONTENT_TYPE'] ?? '') == "application/json")
            $_POST = json_decode(file_get_contents("php://input"), true);

        if(!$this->check_csrf()){
            $result = $this->err('Wrong CSRF token.', 401);
            return $respond($result);
        }

        if($method = $this->get_method()){
            try {
                $params = $this->validate_method_call($method);
                if(!$result = $this->before()){
                    $result = $this->$method(...$params);
                    $this->after();
                }
            } catch(RouterException $ex){
                $result = $this->err($ex->getMessage(), $ex->getCode());
            } catch(Throwable $ex){
                $result = $this->err($ex->getMessage(), 500);
                $this->process_exception($ex);
            }
        } else $result = $this->not_found("Endpoint '$this->action' for method $_SERVER[REQUEST_METHOD] not found.");

        return $respond($result);
    }

    /**
     * Return an array indicating successful processing of the call.
     * @param array $fields fields to send additionally.
     * @param string $message
     * @return array
     */
    public function ok(array $fields = [], string $message = 'ok'){
        $fields['status']  = 200;
        $fields['success'] = true;
        $fields['message'] = $message;
        return $fields;
    }

    /**
     * Return an array indicating error when processing the call.
     * PageRouter's equivalent is called error() however using err()
     * here for the brevity of the return statements.
     * @param $message string error message.
     * @param $error_code int error code.
     * @param array $append optional array to send.
     * @return array
     */
    public function err(string $message, int $error_code, array $append = []){
        $append['message'] = $message;
        $append['status']  = $error_code;
        $append['success'] = $error_code == 200;
        $this->set_status($error_code, $message);
        return $append;
    }

    /**
     * Produce a 404 not found error array. PagerRouter's equivalent
     * is called error_404() because it is the most common error page.
     * The ajax version is called _not_found()_ because calling it 404
     * for brevity is not possible in php.
     * @param $message string
     * @return array
     */
    public function not_found(string $message = "Not found"){
        return $this->err($message, 404);
    }
}