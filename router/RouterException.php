<?php

namespace glowbug\router;

class RouterException extends \Exception {
    public function __construct($message, $code = 404, \Throwable $previous = null){ parent::__construct($message, intval($code), $previous); }

    public function is_404(){
        return $this->code == 404;
    }
}