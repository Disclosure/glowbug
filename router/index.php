<?php

/// typical index.php example

/*
the below to handle that on webserver level, however
there are some issues with it as it overrides url
parsing completely:

location ~* "\.\w{1,12}$" {
    try_files $uri =404;
}

 */

$url = $_SERVER['REQUEST_URI'];
/// cut off file requests early
if(preg_match('/\.[a-zA-Z]{1,4}$/', $url) === 1){
    http_response_code(404);
    die("File not found.");
}

use routers\Ajax;
use routers\Main;
use routers\Test;
use glowbug\router\Dispatcher;
use dbs\DB;

require_once "../../autoload.php";

$db         = new DB;
$routes     = [
    'Main' => [$db],
    'Ajax' => [$db],
    'Test' => [],
];
$dispatcher = new Dispatcher($routes, 'routers\\');

/** @var Main|Ajax|Test $router */
$router = $dispatcher->dispatch($url);
$router->route();
